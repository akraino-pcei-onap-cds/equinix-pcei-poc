package com.aarna.composite.app.deploy.scripts.utils

import com.fasterxml.jackson.annotation.JsonAlias

class Networks {
    @JsonAlias("network-name")
    var name: String = ""
    @JsonAlias("subnets")
    lateinit var subnets: ArrayList<Subnets>
}
