/*
 * Copyright © 2021 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aarna.composite.app.deploy.scripts

import org.slf4j.LoggerFactory

/**
 * Constant file
 */
class CompositeAppDeployServiceConstants {

    companion object {

        private val log = LoggerFactory.getLogger(
            CompositeAppDeployServiceConstants::class.java
        )

        const val VAR_SIMPLE_COMPOSITE_APP_REQUEST_REQUEST =
            "simple-composite-app-deploy-workflow-request"
        const val VAR_COMPLEX_COMPOSITE_APP_REQUEST_REQUEST =
            "complex-composite-app-deploy-workflow-request"
        const val VAR_AMCOP_MIDDLE_END_PROPERTIES =
            "amcop-middle-end-properties"
        const val VAR_AMCOP_MIDDLE_END_URL =
            "deploy-intent-group-api-url"
        const val VAR_PROJECT_NAME =
            "amcop-service-project-name"
        const val VAR_INPUT_PARAMS =
            "composite-app-deploy-input-params"
        const val VAR_COMPOSITE_APP_PARAMS =
            "composite-application-parameters"

        const val VAR_TARGET_APPS_PARAMS =
            "application-data-parameters"

        const val VAR_APPS_OVERRIDE_PARAMS =
            "application-override-parameters"

        const val VAR_TARGET_CLUSTER_PARAMETERS =
            "target-cluster-parameters"
        const val VAR_APP_NETWORK_PARAMS =
            "app-network-parameters"
        const val VAR_APP_INTERFACES_PARAMS =
            "app-interfaces-parameters"
        const val VAR_APP_METADATA_PARAMS =
            "app-metadata-parameters"
        const val VAR_TARGET_CLUSTER_PROV_PARAMS =
            "target-cluster-provider-parameters"

        const val VAR_REQ_COMPOSITE_APP = "composite-app"
        const val VAR_REQ_COMPOSITE_APP_VERSION = "composite-app-version"
        const val VAR_REQ_COMPOSITE_APP_PROFILE = "composite-profile"
        const val VAR_REQ_NAME = "service-instance-name"
        const val VAR_REQ_DESCRIPTION = "service-instance-description"
        const val VAR_REQ_VERSION = "service-instance-version"

        const val VAR_CLUSTER_DEPLOY_INTENT = "/middleend/projects"

        const val VAR_V2_CLUSTER_DEPLOY_INTENT = "/v2/projects"

        const val VAR_CLUSTER_COMPOSITE_APP = "/composite-apps"
        const val VAR_CLUSTER_DEPLOY_INTENT_GROUP = "/deployment-intent-groups"

        const val VAR_METADATA = "metadata"
        const val VAR_METADATA_NAME = "name"
        const val VAR_SUCCESS = "success"
        const val VAR_FAIL = "fail"
        const val VAR_ERROR_DATA = "error-data"

        const val PAYLOAD_SELECTED_CLUSTERS_NAME = "name"
        const val VAR_SPEC = "spec"
        const val VAR_APPS_DATA = "appsData"
        const val PAYLOAD_SELECTED_CLUSTERS = "selectedClusters"
        const val PAYLOAD_CLUSTERS = "clusters"
        const val REQ_CLUSTERS = "clusters"
        const val REQ_CLUSTER_PROV_NAME = "cluster-provider-name"
        const val PAYLOAD_PROVIDER = "provider"
        const val VAR_INTERFACES_PAYLOAD = "{\n" +
            "\t\"networkName\": \"INTERFACES_NETWORKNAME\",\n" +
            "\t\"ip\": \"INTERFACES_IP\",\n" +
            "\t\"subnet\": \"INTERFACES_SUBNET\"\n" +
            "}"
        const val VAR_METADATA_PAYLOAD = "{\n" +
            "\t\"name\": \"METADATA_NAME\",\n" +
            "\t\"description\": \"METADATA_DESCRIPTION\",\n" +
            "\t\"UserData1\": \"METADATA_USERDATA1\",\n" +
            "\t\"UserData2\": \"METADATA_USERDATA2\",\n" +
            "\t\"chartContent\": \"METADATA_CHARTCONTENT\",\n" +
            "\t\"status\": \"METADATA_STATUS\"\n" +
            "}"
        const val VAR_COMPOSITE_APP = "compositeApp"
        const val VAR_COMPOSITE_APP_VERSION = "compositeAppVersion"
        const val VAR_COMPOSITE_PROFILE = "compositeProfile"
        const val VAR_DESCRIPTION = "description"
        const val VAR_NAME = "name"
        const val VAR_VERSION = "version"
        const val PAYLOAD_PROJECT_NAME = "projectName"
        const val PAYLOAD_OVERRIDE_VALUES = "projectName"
        const val REQ_META_APP_NAME = "app-name"
        const val REQ_META_APP_DESC = "app-description"
        const val REQ_META_DATA1 = "app-user-data1"
        const val REQ_META_DATA2 = "app-user-data2"
        const val PAYLOAD_META_APP_NAME = "name"
        const val PAYLOAD_META_APP_DESC = "description"
        const val PAYLOAD_META_DATA1 = "UserData1"
        const val PAYLOAD_META_DATA2 = "UserData2"

        const val REQ_NETWORK_NAME = "network-name"
        const val REQ_NETWORK_SUBNET_NAME = "subnet-name"
        const val REQ_NETWORK_SUBNET_CIDR = "subnet-cidr-range"
        const val PAYLOAD_NETWORK_NAME = "networkName"
        const val PAYLOAD_NETWORK_IP = "ip"
        const val PAYLOAD_NETWORK_SUBNET = "subnet"
        const val PAYLOAD_INTERFACES = "interfaces"

        const val REQ_OVERRIDE_APP_NAME = "app-name"
        const val REQ_OVERRIDE_APP_VALUES = "values"
        const val PAYLOAD_OVERRIDE_APP_NAME = "app-name"
        const val PAYLOAD_OVERRIDE_APP_VALUES = "values"
    }
}
