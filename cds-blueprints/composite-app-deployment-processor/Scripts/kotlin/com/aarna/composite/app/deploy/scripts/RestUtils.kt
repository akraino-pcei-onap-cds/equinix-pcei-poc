/*
 * Copyright © 2021 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aarna.composite.app.deploy.scripts

import com.aarna.composite.app.deploy.scripts.utils.AppsData
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.onap.ccsdk.cds.controllerblueprints.core.BluePrintException
import org.slf4j.LoggerFactory
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.util.ResourceUtils
import java.io.File
import java.io.FileInputStream
import java.util.HashMap

/**
 * Constant file
 */
class RestUtils {

    companion object {

        private val log = LoggerFactory.getLogger(
            RestUtils::class.java
        )

        const val VAR_SUCCESS = "success"
        const val VAR_FAIL = "fail"

        /**
         * utility with multiple api calls to create a service instance
         */
        fun compositeRestPost(
            requestUrl: String,
            v2Url: String,
            payloadRequest: String
        ): HashMap<String, String> {
            log.info(" Composite REST POST started")

            var postRes: HashMap<String, String> = restPost(
                requestUrl, payloadRequest
            )

            val tempApproveUrl: String = "$v2Url/approve"
            log.info(" Approve Post URL $tempApproveUrl")
            val tempPayload: String = ""

            val tempInstantiateUrl: String = "$v2Url/instantiate"
            log.info(" Instantiate Post URL $tempInstantiateUrl")

            if (postRes.get("status") == VAR_SUCCESS) {
                postRes = restPost(tempApproveUrl, tempPayload)
                if (postRes.get("status") == VAR_SUCCESS) {
                    postRes = restPost(tempInstantiateUrl, tempPayload)
                }
            }
            // return status code
            return postRes
        }

        /**
         * Restful Post utility method
         */
        fun restPost(requestUrl: String, payloadRequest: String):
            HashMap<String, String> {
                log.info(" REST POST started")
                var tempMap: HashMap<String, String> = HashMap()
                // construct cluster providers rest API
                val builder = RestTemplateBuilder()
                val headers = HttpHeaders()

                headers.setContentType(MediaType.APPLICATION_JSON)
                val entity: HttpEntity<String> = HttpEntity(
                    payloadRequest, headers
                )
                try {
                    val response: ResponseEntity<String> = builder.build()
                        .exchange(
                            requestUrl, HttpMethod.POST, entity,
                            String::class.java
                        )
                    val statusCode: Int = response.statusCodeValue
                    log.info("result :==: $statusCode")
                    tempMap.put("httpStatusCode", statusCode.toString())
                    if ((statusCode == 201) or
                        (statusCode == 202)
                    ) {
                        tempMap.put("status", VAR_SUCCESS)
                    } else {
                        tempMap.put("status", VAR_FAIL)
                    }
                    tempMap.put("httpResponse", entity.toString())
                    return tempMap
                } catch (blueprintExp: BluePrintException) {
                    log.info("BlueprintException : = $blueprintExp")
                    tempMap.put("httpResponse", "${blueprintExp.message}")
                } catch (exception: Exception) {
                    log.info("Exception : = $exception")
                    tempMap.put("httpResponse", "${exception.message}")
                }
                return tempMap
            }

        /**
         * upload file utility method
         */
        fun uploadPost(
            regUrl: String,
            fileUrl: String,
            payloadRequest: String
        ): HashMap<String, String> {
            var tempMap: HashMap<String, String> = HashMap()

            val file: File = ResourceUtils.getFile("$fileUrl")
            val httpClient: CloseableHttpClient = HttpClients.createDefault()
            val uploadFile = HttpPost(regUrl)
            val builder = MultipartEntityBuilder.create()
            builder.addTextBody(
                "metadata", payloadRequest, ContentType.TEXT_PLAIN
            )

            // This attaches the file to the POST:
            builder.addBinaryBody(
                "file",
                FileInputStream(file),
                ContentType.APPLICATION_OCTET_STREAM,
                file.name
            )
            val multipart = builder.build()
            uploadFile.entity = multipart
            try {
                val response: CloseableHttpResponse =
                    httpClient.execute(uploadFile)
                val responseEntity: org.apache.http.HttpEntity =
                    response.getEntity()

                val statusCode = response.statusLine.statusCode
                log.info("StatusCode := $statusCode")
                log.info("StatusLine := ${response.statusLine}")
                log.info("responseEntity := $responseEntity")
                log.info("responseEntity content := ${responseEntity.content}")
                tempMap.put("httpStatusCode", statusCode.toString())
                if (statusCode == 201) {
                    tempMap.put("status", VAR_SUCCESS)
                } else {
                    tempMap.put("status", VAR_FAIL)
                }
                tempMap.put("httpResponse", responseEntity.toString())
                return tempMap
            } catch (blueprintExp: BluePrintException) {
                log.info("BlueprintException : = $blueprintExp")
                tempMap.put("httpResponse", "${blueprintExp.message}")
            } catch (exception: Exception) {
                log.info("Exception : = $exception")
                tempMap.put("httpResponse", "${exception.message}")
            }
            return tempMap
        }

        /**
         * utility to parse and construct the Network json from payload
         */
        fun parseAndContructNetworkJson(
            networkpayloadRequest: String
        ): JsonElement {
            // convert Json array to Map and map this to Network payload
            val arrayListNetworkParams = object :
                TypeToken<ArrayList<MutableMap<String, String>>>() {}
                .type
            var tempList: ArrayList<MutableMap<String, String>> = Gson()
                .fromJson(networkpayloadRequest, arrayListNetworkParams)

            val mapNetworkParams = object : TypeToken<MutableMap<String,
                    String>>() {}
                .type

            var tempIpList: ArrayList<MutableMap<String, String>> =
                ArrayList<MutableMap<String, String>> ()
            var tempMap: MutableMap<String, String>
            for (ipTemp in tempList) {
                tempMap = Gson().fromJson(
                    CompositeAppDeployServiceConstants.VAR_INTERFACES_PAYLOAD,
                    mapNetworkParams
                )
                tempMap[CompositeAppDeployServiceConstants.PAYLOAD_NETWORK_NAME] =
                    ipTemp.get(
                        CompositeAppDeployServiceConstants.REQ_NETWORK_NAME
                    ).toString()
                tempMap[CompositeAppDeployServiceConstants.PAYLOAD_NETWORK_IP] =
                    ipTemp.get(
                        CompositeAppDeployServiceConstants.REQ_NETWORK_SUBNET_NAME
                    ).toString()
                tempMap[CompositeAppDeployServiceConstants.PAYLOAD_NETWORK_SUBNET] =
                    ipTemp.get(
                        CompositeAppDeployServiceConstants.REQ_NETWORK_SUBNET_CIDR
                    ).toString()

                tempIpList.add(tempMap)
            }
            log.info("Network Payload request")
            log.info(tempIpList.toString())
            // convert map to Json string
            return Gson().toJsonTree(tempIpList)
        }

        /**
         * utility to parse and construct the metadata json from payload
         */
        fun parseAndContructMetadataJson(
            metadataPayloadRequest: String
        ): JsonElement {
            var tempMap: MutableMap<String, String> = HashMap()
            val metadataParams = object : TypeToken<MutableMap<String,
                    Any>>() {}
                .type
            var tempList: MutableMap<String, String> = Gson()
                .fromJson(metadataPayloadRequest, metadataParams)
            tempMap[CompositeAppDeployServiceConstants.PAYLOAD_META_APP_NAME] =
                tempList.get(
                    CompositeAppDeployServiceConstants.REQ_META_APP_NAME
                ).toString()
            tempMap[CompositeAppDeployServiceConstants.PAYLOAD_META_APP_DESC] =
                tempList.get(
                    CompositeAppDeployServiceConstants.REQ_META_APP_DESC
                ).toString()
            tempMap[CompositeAppDeployServiceConstants.PAYLOAD_META_DATA1] =
                tempList.get(
                    CompositeAppDeployServiceConstants.REQ_META_DATA1
                ).toString()
            tempMap[CompositeAppDeployServiceConstants.PAYLOAD_META_DATA2] =
                tempList.get(
                    CompositeAppDeployServiceConstants.REQ_META_DATA2
                ).toString()
            return Gson().toJsonTree(tempMap)
        }

        /**
         * utility to construct the appsData json
         */
        fun simpleAppsDataPayload(
            appsParams: String
        ): JsonElement {
            // construct apps json
            val arrayListAppsParams = object :
                TypeToken<ArrayList<MutableMap<String, Any>>>() {}
                .type
            var tempAppsList: ArrayList<MutableMap<String, String>> =
                Gson().fromJson(appsParams, arrayListAppsParams)

            var tempAppsArray: JsonArray = JsonArray()
            var tempAppsJsonNode: JsonObject = JsonObject()
            var tempMetaDataJsonNode: JsonElement
            var tempClutersJsonNode: JsonElement
            for (appsTemp in tempAppsList) {
                tempAppsJsonNode = JsonObject()
                tempMetaDataJsonNode = parseAndContructMetadataJson(
                    Gson().toJsonTree(
                        appsTemp.get(
                            CompositeAppDeployServiceConstants.VAR_APP_METADATA_PARAMS
                        )
                    ).toString()
                )
                tempClutersJsonNode = parseAndContructClusterProvidersJson(
                    appsTemp
                        .get(
                            CompositeAppDeployServiceConstants.VAR_TARGET_CLUSTER_PROV_PARAMS
                        ).toString()
                )
                tempAppsJsonNode.add(
                    CompositeAppDeployServiceConstants.VAR_METADATA,
                    tempMetaDataJsonNode
                )
                tempAppsJsonNode.add(
                    CompositeAppDeployServiceConstants.PAYLOAD_CLUSTERS,
                    tempClutersJsonNode
                )
                tempAppsArray.add(tempAppsJsonNode)
            }
            log.info("constructAppsDataRequestPayload ")
            log.info(tempAppsArray.toString())
            // convert map to Json string
            return tempAppsArray
        }

        /**
         * utility to constract cluster json
         */
        fun parseAndContructClusterJson(
            clusterPayloadRequest: String
        ): JsonElement {
            val arrayListClusterParams = object :
                TypeToken<ArrayList<MutableMap<String, Any>>>() {}
                .type
            var tempList: ArrayList<MutableMap<String, String>> =
                Gson().fromJson(clusterPayloadRequest, arrayListClusterParams)
            var tempClusters: JsonArray = JsonArray()
            var tempJsonNode: JsonObject = JsonObject()
            var tempNetworkJsonNode: JsonElement
            for (clusterTemp in tempList) {
                tempJsonNode = JsonObject()
                tempJsonNode.addProperty(
                    CompositeAppDeployServiceConstants
                        .PAYLOAD_SELECTED_CLUSTERS_NAME,
                    clusterTemp.get("name").toString()
                )
                if (clusterTemp.get(
                        CompositeAppDeployServiceConstants.VAR_APP_INTERFACES_PARAMS
                    ) != null
                ) {
                    tempNetworkJsonNode = parseAndContructNetworkJson(
                        clusterTemp
                            .get(CompositeAppDeployServiceConstants.VAR_APP_INTERFACES_PARAMS)
                            .toString()
                    )
                    tempJsonNode.add(
                        CompositeAppDeployServiceConstants.PAYLOAD_INTERFACES,
                        Gson().toJsonTree(tempNetworkJsonNode)
                    )
                }
                tempClusters.add(tempJsonNode)
            }
            return tempClusters
        }

        /**
         * utility to contruct the cluster providers Json
         */
        fun parseAndContructClusterProvidersJson(
            clusterPayloadRequest: String
        ): JsonElement {
            val arrayListClusterParams = object :
                TypeToken<ArrayList<MutableMap<String, Any>>>() {}
                .type
            var tempList: ArrayList<MutableMap<String, String>> =
                Gson().fromJson(clusterPayloadRequest, arrayListClusterParams)
            var tempProvClusters: JsonArray = JsonArray()
            var tempJsonNode: JsonObject = JsonObject()
            var tempClutersJsonNode: JsonElement
            for (clusterTemp in tempList) {
                tempJsonNode = JsonObject()
                tempJsonNode.addProperty(
                    CompositeAppDeployServiceConstants.PAYLOAD_PROVIDER,
                    clusterTemp.get(
                        CompositeAppDeployServiceConstants.REQ_CLUSTER_PROV_NAME
                    ).toString()
                )
                tempClutersJsonNode = parseAndContructClusterJson(
                    clusterTemp.get(
                        CompositeAppDeployServiceConstants.REQ_CLUSTERS
                    ).toString()
                )
                tempJsonNode.add(
                    CompositeAppDeployServiceConstants.PAYLOAD_SELECTED_CLUSTERS,
                    Gson().toJsonTree(tempClutersJsonNode)
                )
                tempProvClusters.add(tempJsonNode)
            }
            return tempProvClusters
        }

        /**
         * utility to construct the complex appsData json
         */
        fun complexAppsDataPayload(
            appsParams: String
        ): ArrayList<AppsData> {
            var mapper = ObjectMapper()
            var tempAppsArray: ArrayList<AppsData> = ArrayList<AppsData>()
            val arrayJnode: JsonNode = mapper.readTree(appsParams) as ArrayNode
            var apps: AppsData
            for (tempNode in arrayJnode) {
                log.info(tempNode.toPrettyString())
                apps = mapper.readValue(
                    tempNode.toPrettyString(), AppsData::class.java
                )
                tempAppsArray.add(apps)
            }
            log.info("Request Apps Payload ")
            log.info(tempAppsArray.toString())
            return tempAppsArray
        }

        /**
         * utility to construct the override json
         */
        fun parseAndContructOverrideJson(
            overrideParams: String
        ): JsonElement {
            // convert Json array to Map and map this to Network payload
            val arrayListOverrideParams = object :
                TypeToken<ArrayList<MutableMap<String, Any>>>() {}
                .type
            var tempList: ArrayList<MutableMap<String, Any>> = Gson()
                .fromJson(overrideParams, arrayListOverrideParams)

            var tempOverrideArray: JsonArray = JsonArray()
            var tempOverrideNode: JsonObject = JsonObject()

            for (overrideTemp in tempList) {
                tempOverrideNode = JsonObject()
                tempOverrideNode.addProperty(
                    CompositeAppDeployServiceConstants
                    .PAYLOAD_OVERRIDE_APP_NAME, overrideTemp.get
                (CompositeAppDeployServiceConstants.REQ_OVERRIDE_APP_NAME).toString())
                tempOverrideNode.add(
                    CompositeAppDeployServiceConstants.PAYLOAD_OVERRIDE_APP_VALUES,
                    Gson().toJsonTree(overrideTemp.get
                        (CompositeAppDeployServiceConstants.REQ_OVERRIDE_APP_VALUES))
                )
                tempOverrideArray.add(tempOverrideNode)
            }
            log.info("Override Payload request")
            log.info(tempOverrideArray.toString())
            // convert map to Json string
            return tempOverrideArray
        }
    }
}
