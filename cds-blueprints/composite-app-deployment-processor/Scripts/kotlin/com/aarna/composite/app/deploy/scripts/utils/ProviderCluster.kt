package com.aarna.composite.app.deploy.scripts.utils
import com.fasterxml.jackson.annotation.JsonAlias

class ProviderCluster {

    @JsonAlias("cluster-provider-name")
    var provider: String = ""
    @JsonAlias("clusters")
    lateinit var selectedClusters: ArrayList<Clusters>
}
