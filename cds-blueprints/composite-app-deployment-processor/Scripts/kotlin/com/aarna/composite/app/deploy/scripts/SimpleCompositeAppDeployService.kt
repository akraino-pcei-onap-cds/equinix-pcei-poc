/*
 * Copyright © 2021 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aarna.composite.app.deploy.scripts

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import org.onap.ccsdk.cds.blueprintsprocessor.core.api.data.ExecutionServiceInput
import org.onap.ccsdk.cds.blueprintsprocessor.services.execution.AbstractScriptComponentFunction
import org.onap.ccsdk.cds.controllerblueprints.core.BluePrintException
import org.onap.ccsdk.cds.controllerblueprints.core.asJsonType
import org.slf4j.LoggerFactory

/**
 * Service API class which creates a cluster provider
 */
open class SimpleCompositeAppDeployService : AbstractScriptComponentFunction() {

    private val log = LoggerFactory
        .getLogger(SimpleCompositeAppDeployService::class.java)

    /**
     * @see ExecutionServiceInput
     */
    override suspend fun processNB(executionRequest: ExecutionServiceInput) {
        val requestPayload = executionRequest.payload
        log.info("Step : =")
        log.info("Execution Request : $requestPayload")
        // extract the request parameters from request payload
        val tempRequest = requestPayload
            .path(CompositeAppDeployServiceConstants.VAR_SIMPLE_COMPOSITE_APP_REQUEST_REQUEST)
        val tempInputParams = tempRequest
            .path(CompositeAppDeployServiceConstants.VAR_INPUT_PARAMS)
        val tempAmcopProp = tempRequest
            .path(CompositeAppDeployServiceConstants.VAR_AMCOP_MIDDLE_END_PROPERTIES)
        val tempAmcopAppUrl = tempAmcopProp
            .path(
                CompositeAppDeployServiceConstants
                    .VAR_AMCOP_MIDDLE_END_URL
            ).textValue()
        val tempProjectName = tempRequest
            .path(CompositeAppDeployServiceConstants.VAR_PROJECT_NAME).textValue()
        val tempClusterDeployParams = tempInputParams
            .path(CompositeAppDeployServiceConstants.VAR_COMPOSITE_APP_PARAMS)
        val tempAppsParams = tempInputParams
            .path(CompositeAppDeployServiceConstants.VAR_TARGET_APPS_PARAMS)

        val tempAppsOverrideParams = tempInputParams
            .path(CompositeAppDeployServiceConstants.VAR_APPS_OVERRIDE_PARAMS)

        val tempCompositeAppName = tempClusterDeployParams
            .path(CompositeAppDeployServiceConstants.VAR_REQ_COMPOSITE_APP).textValue()
        val tempCompositeAppVersionName = tempClusterDeployParams
            .path(CompositeAppDeployServiceConstants.VAR_REQ_COMPOSITE_APP_VERSION)
            .textValue()
        val tempCompositeAppProfile = tempClusterDeployParams
            .path(CompositeAppDeployServiceConstants.VAR_REQ_COMPOSITE_APP_PROFILE)
            .textValue()
        val tempName = tempClusterDeployParams
            .path(CompositeAppDeployServiceConstants.VAR_REQ_NAME)
            .textValue()
        val tempDescription = tempClusterDeployParams
            .path(CompositeAppDeployServiceConstants.VAR_REQ_DESCRIPTION)
            .textValue()
        val tempVersion = tempClusterDeployParams
            .path(CompositeAppDeployServiceConstants.VAR_REQ_VERSION)
            .textValue()
        val clusterDeployUrl: String =
            CompositeAppDeployServiceConstants.VAR_CLUSTER_DEPLOY_INTENT
        val tempClusterComp: String =
            CompositeAppDeployServiceConstants.VAR_CLUSTER_COMPOSITE_APP
        val tempClusterIntent: String =
            CompositeAppDeployServiceConstants.VAR_CLUSTER_DEPLOY_INTENT_GROUP

        val uiDeployUrl: String =
            CompositeAppDeployServiceConstants.VAR_V2_CLUSTER_DEPLOY_INTENT

        val clusterUrl: String =
            "$tempAmcopAppUrl$clusterDeployUrl/$tempProjectName$tempClusterComp" +
                "/$tempCompositeAppName/$tempCompositeAppVersionName" +
                "$tempClusterIntent"

        val v2Url: String =
            "$tempAmcopAppUrl$uiDeployUrl/$tempProjectName$tempClusterComp" +
                "/$tempCompositeAppName/$tempCompositeAppVersionName" +
                "$tempClusterIntent/$tempName"

        log.info("clusterUrl :==: $clusterUrl")

        var specJson: JsonObject = JsonObject()
        specJson.addProperty(
            CompositeAppDeployServiceConstants.PAYLOAD_PROJECT_NAME,
            tempProjectName
        )
        // apps data
        val tempAppsData: JsonElement = RestUtils.simpleAppsDataPayload(
            tempAppsParams.toString()
        )
        specJson.add(
            CompositeAppDeployServiceConstants.VAR_APPS_DATA, tempAppsData
        )
        // Override values
        if (tempAppsOverrideParams != null && !tempAppsOverrideParams.isEmpty) {
            val tempOverrideData: JsonElement = RestUtils
                .parseAndContructOverrideJson(tempAppsOverrideParams.toString())
            specJson.add(
                CompositeAppDeployServiceConstants.PAYLOAD_OVERRIDE_VALUES,
                tempOverrideData
            )
        }

        // construct request payload construct
        var requestJson: JsonObject = JsonObject()
        requestJson.addProperty(
            CompositeAppDeployServiceConstants.VAR_COMPOSITE_APP,
            tempCompositeAppName
        )
        requestJson.addProperty(
            CompositeAppDeployServiceConstants.VAR_COMPOSITE_APP_VERSION,
            tempCompositeAppVersionName
        )
        requestJson.addProperty(
            CompositeAppDeployServiceConstants.VAR_COMPOSITE_PROFILE,
            tempCompositeAppProfile
        )
        requestJson.addProperty(
            CompositeAppDeployServiceConstants.VAR_DESCRIPTION,
            tempDescription
        )
        requestJson.addProperty(
            CompositeAppDeployServiceConstants.VAR_VERSION, tempVersion
        )
        requestJson.addProperty(
            CompositeAppDeployServiceConstants.VAR_NAME, tempName
        )
        requestJson.add(CompositeAppDeployServiceConstants.VAR_SPEC, specJson)
        log.info("Request Payload ")
        log.info(requestJson.toString())

        // Call Post method to create service instance
        val postRes: HashMap<String, String> = RestUtils.compositeRestPost(
            clusterUrl,
            v2Url,
            requestJson.toString()
        )

        if (postRes.isNotEmpty()) {
            log.info("Cluster Created Successfully")
            val gson = Gson()
            val jsonResponse = gson.toJson(postRes)
            log.info("Cluster Created Successfully")
            log.info("Cluster Created Json response : = $jsonResponse")
            if (postRes.get("status") == CompositeAppDeployServiceConstants.VAR_SUCCESS) {
                setAttribute(
                    "response-data", jsonResponse.asJsonType()
                )
                return
            } else {
                setAttribute(
                    "error-data", jsonResponse.asJsonType()
                )
            }
        }
        throw BluePrintException("Cluster deployment failed")
    }
    /**
     * @param runtimeException consists exception details if there any exception
     * @param executionRequest request payload
     * @see ExecutionServiceInput
     */
    override suspend fun recoverNB(
        runtimeException: RuntimeException,
        executionRequest: ExecutionServiceInput
    ) {
        log.info("Executing Recovery")
        addError("${runtimeException.message}")
    }
}
