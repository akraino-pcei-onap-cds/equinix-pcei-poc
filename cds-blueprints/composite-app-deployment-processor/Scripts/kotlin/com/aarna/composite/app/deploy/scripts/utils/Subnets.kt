package com.aarna.composite.app.deploy.scripts.utils

import com.fasterxml.jackson.annotation.JsonAlias

class Subnets {
    @JsonAlias("excludeIps")
    var excludeIps: String = ""
    @JsonAlias("gateway")
    var gateway: String = ""
    @JsonAlias("name")
    var name: String = ""
    @JsonAlias("subnet")
    var subnet: String = ""
}
