/*
 * Copyright © 2017-2021 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aarna.composite.app.deploy.scripts.utils

import com.fasterxml.jackson.annotation.JsonAlias

class Metadata {
    @JsonAlias("app-description")
    var description: String = ""
    @JsonAlias("app-name")
    var name: String = ""
    @JsonAlias("app-user-data1")
    var userData1: String = ""
    @JsonAlias("app-user-data2")
    var userData2: String = ""
}
