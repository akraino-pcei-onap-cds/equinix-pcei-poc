/*
 * Copyright © 2017-2021 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aarna.composite.app.deploy.scripts.utils

import com.fasterxml.jackson.annotation.JsonAlias

class Clusters {
    @JsonAlias("app-available-network-parameters")
    lateinit var availableNetworks: ArrayList<Networks>
    @JsonAlias("app-interfaces-parameters")
    lateinit var interfaces: ArrayList<Interfaces>
    @JsonAlias("app-network-parameters")
    lateinit var networks: ArrayList<Networks>
    @JsonAlias("name")
    var name: String = ""
}
