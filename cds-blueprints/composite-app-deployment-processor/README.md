# Step by step instructions to load this CBA

# Assumptions
  1. CDS up and running 
  
    :~$ kubectl get pods -n onap | grep cds
	dev-cds-blueprints-processor-b8656f584-wn6lh   1/1     Running    6          7d3h
	dev-cds-command-executor-78b98dff4f-8mtnk      1/1     Running    6          7d3h
	dev-cds-db-0                                   1/1     Running    6          7d3h
	dev-cds-py-executor-8499669b99-ph84w           1/1     Running    6          7d3h
	dev-cds-sdc-listener-749896d795-qgp2t          0/1     Init:0/1   350        7d3h
	dev-cds-ui-77f74ff6db-gbgwp                    1/1     Running    7          7d3h


  2. AMCOP 2.0 up and running. Note down AMCOP IP and port no. as this need to be updated in 
     request payload 
	
	ubuntu@node1:~$ kubectl get svc -n onap4k8s
	NAME           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                           AGE
	cdap           NodePort    10.233.39.66    <none>        11011:31011/TCP,11015:31015/TCP   62d
	clm            ClusterIP   10.233.11.58    <none>        9061/TCP                          62d
	configsvc      NodePort    10.233.26.91    <none>        9082:30482/TCP                    62d
	dmaap          NodePort    10.233.8.195    <none>        3904:32392/TCP,3905:30768/TCP     62d
	**emcoui         NodePort    10.233.3.164    <none>        9080:30480/TCP                    62d**
	etcd           ClusterIP   10.233.21.72    <none>        2379/TCP,2380/TCP                 62d
	kafka1         ClusterIP   10.233.58.240   <none>        9092/TCP                          62d
	middleend      NodePort    10.233.37.191   <none>        9081:30481/TCP                    62d
	mongo          ClusterIP   10.233.8.236    <none>        27017/TCP                         62d
	ncm            ClusterIP   10.233.56.217   <none>        9031/TCP                          62d
	orchestrator   ClusterIP   10.233.41.158   <none>        9015/TCP                          62d
	ovnaction      ClusterIP   10.233.2.121    <none>        9053/TCP,9051/TCP                 62d
	rsync          ClusterIP   10.233.40.22    <none>        9041/TCP                          62d
	vescollector   NodePort    10.233.43.127   <none>        8080:31080/TCP                    62d
	zookeeper      ClusterIP   10.233.36.23    <none>        2181/TCP                          62d

  3. Assumption is that you had extracted or cloned the git repository under the $HOME folder
  
# Prerequisite
  1. Create K8s Controller 'rsync'
  
     AMCOP2.0 UI -> K8s Controllers -> Register Controller
  
     ![](images/controller_rsync_create.png)
     
     ![](images/controller_rsync.png)
  
  2. Service Project Name provided in request payload must exist in AMCOP 2.0
  
     AMCOP2.0 UI -> Tenants -> Add Tenant
  
     ![](images/Create_Tenant.png)
     
     ![](images/Tenants.png)  
  
  3. Service Name provided in request payload must exist in AMCOP 2.0
     
     AMCOP2.0 UI -> Tenants - > select Tenant -> Services
    
     ![](images/Serivce_Instances.png)    
   

# Look into the data dictionary dd.json

	$ cd ~/equinix-pcei-poc/cds-blueprints/composite-app-deployment-processor

	$ cat Scripts/dd.json

# Load the dd.json into CDS

	$ cd ../../util-scripts

	$ bash -x ./dd-microk8s.sh ../cds-blueprints/composite-app-deployment-processor/Scripts/dd.json

# Create CBA Archive

	$ cd ../cds-blueprints/composite-app-deployment-processor

	$ zip -r composite-app-deployment-processor.zip *

# Enrich CBA Archive. It makes it portable to future CDS runtime releases
	
	$ cd ../../util-scripts

	$ bash -x ./enrich-and-download-cds-blueprint.sh ../cds-blueprints/composite-app-deployment-processor/composite-app-deployment-processor.zip

# Save/Deploy the CBA Archive into the CDS runtime (CDS desing time activity ends here)

	$ bash -x ./save-enriched-blueprint.sh /tmp/CBA/ENRICHED-CBA.zip

# Open a new SSH terminal to tail CDS blueprint processor logs

	$ cd ~/equinix-pcei-poc/util-scripts

	$ ./tail-cds-bp-log.sh

# Update the Request JSON payload

	$ cd ~/equinix-pcei-poc/cds-blueprints/composite-app-deployment-processor/Scripts

# Copy the template file
	$ cp simple-composite-app-deploy-template.json request-payload.json
	
# Sample request payload
    # You can refer to the below AMCOP REST API link to get more details about the JSON payload
	# https://wiki.onap.org/display/DW/V2+API+Specification
    # Note:- You should update the attribute values for USER_INPUT_XXX parameters
	
[simple-composite-app-deploy-template.json](Scripts/simple-composite-app-deploy-template.json)
	
    $ vi request-payload.json
	{
	  "actionIdentifiers": {
		"mode": "sync",
		"blueprintName": "composite-app-deploy-processor",
		"blueprintVersion": "1.0.0",
		"actionName": "simple-composite-app-deploy-workflow"
	  },
	  "commonHeader": {
        "subRequestId": "143748f9-3cd5-4910-81c9-a4601ff2dc58",
        "requestId": "e5eb1f1e-3386-435d-b290-d49d8af8da4c",
		"originatorId": "SDNC_DG"
	  },
	  "payload": {
		"simple-composite-app-deploy-workflow-request": {
		  "composite-app-deploy-input-params": {
			"composite-application-parameters": {
			  "service-instance-name": "USER_INPUT_TARGET_CLUSTER_NAME",
			  "service-instance-description": "USER_INPUT_TARGET_CLUSTER_DESCRIPTION",
			  "service-instance-version": "USER_INPUT_TARGET_CLUSTER_VERSION",
			  "composite-app": "USER_INPUT_COMPOSITE_APP_NAME",
			  "composite-app-version": "USER_INPUT_COMPOSITE_APP_VERSION",
			  "composite-profile": "USER_INPUT_COMPOSITE_PROFILE"
			},
			"application-data-parameters": [{
			  "app-metadata-parameters": {
				"app-name": "USER_INPUT_APP_NAME"
			  },
			  "target-cluster-provider-parameters": [{
				"cluster-provider-name": "USER_INPUT_PROVIDER_NAME",
				"clusters": [{
				  "name": "USER_INPUT_CLUSTER_NAME"
				}]
			  }]
			}],
    		"application-override-parameters": [{
				"app-name": "USER_INPUT_APP_NAME",
    			"values": {
    				"USER_INPUT_VARIABLE_NAME": "USER_INPUT_VARIABLE_VALUE"
    			}
			}]
		    },
    		"amcop-middle-end-properties": {
    			"deploy-intent-group-api-url": "USER_INPUT_AMCOP_APP_API_URL",
    			"amcop-middle-end-username": "",
    			"amcop-middle-end-password": ""
    		},
		    "amcop-service-project-name": "USER_INPUT_AMCOP_SERVICE_PROJECT_NAME"
		}
	  }
	}

# Update the request-payload.json
	$ cat request-payload.json
	$ export REQUEST_PAYLOAD="$PWD/request-payload.json"

# Get the CDS blueprint processor service IP
	$ CDS_BP_SVC_IP=$(kubectl get svc -n onap | grep 'cds-blueprints-processor-http' | awk '{print $3}')

	$ echo "CDS Blueprint processor IP = $CDS_BP_SVC_IP"

# Execute the below CURL Command to execute this blueprint
    # On success the below command will return HTTP status 200

	$ curl -v --location --request POST http://${CDS_BP_SVC_IP}:8080/api/v1/execution-service/process --header 'Content-Type: application/json;charset=UTF-8' --header 'Accept: application/json;charset=UTF-8,application/json' --header 'Authorization: Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==' --header 'Host: cds-blueprints-processor-http:8080' --header 'Content-Type: text/json' --data  "@$REQUEST_PAYLOAD" | python3 -m json.tool

# Now you can switch to the CDS blueprint processor log console for more information
	
# Look at the AMCOP 2.0 console UI to verify cluster provider displayed.

	http://AMCOP_PORTAL_IP:AMCOP_PORTAL_PORT_NUM/app/projects 

# Application Deployment with Secondary Networks

1. CNI deployment on target network

	# Option1 : 
	Run aio.sh script on the target clusters, which deploys the k8s and other components. 
	This includes the deployment of the ovn4nfv CNI. 

		#Execute below commands in the VM 
	
		sudo apt-get update -y

		sudo apt-get upgrade -y

		sudo apt-get install -y python-pip

		Note: Make sure Python 3.x is the default python version on the server.
		
		git clone https://git.onap.org/multicloud/k8s/

		# Run script to setup KUD
		nohup k8s/kud/hosting_providers/baremetal/aio.sh &

 		
	# Option2 : 
	Deploy multus & ovn4nfv at edge cluster

    multus : 

		#Execute below commands 
		git clone https://github.com/intel/multus-cni.git
		cd multus-cni
		cat ./images/multus-daemonset.yml | kubectl delete -f -
		sleep 1m  
		cat ./images/multus-daemonset.yml | kubectl apply -f -
    
	ovn4nfv:

		#Refer below to install ovn4nfv CNI 
	
		git clone https://git.onap.org/multicloud/k8s/
		cd Multicloudk8s/kud
		cat README.md
		cd Multicloudk8s/kud/deployment_infra/playbooks
	       
![](images/ovn4nfv_readme.png)
	
 
  
  2. Create Secondary Networks in AMCOP UI
  
     AMCOP2.0 UI -> Clusters -> Select created Cloud Provider -> Add Network 
  
     ![](images/cluster_add_network.png)


	 AMCOP2.0 UI -> Clusters -> Select created Cloud Provider
     
     ![](images/cluster_networks.png)
     


  3. Sample complex request payload 
  
  	#You can refer to the below AMCOP REST API link to get more details about the JSON payload
    #https://wiki.onap.org/display/DW/V2+API+Specification
	#Note:- You should update the attribute values for USER_INPUT_XXX parameters
	
[complex-composite-app-deploy-template.json](Scripts/complex-composite-app-deploy-template.json)
	
    $ vi request-payload.json
	{
	  "actionIdentifiers": {
		"mode": "sync",
		"blueprintName": "composite-app-deploy-processor",
		"blueprintVersion": "1.0.0",
		"actionName": "simple-composite-app-deploy-workflow"
	  },
	  "commonHeader": {
        "subRequestId": "143748f9-3cd5-4910-81c9-a4601ff2dc58",
        "requestId": "e5eb1f1e-3386-435d-b290-d49d8af8da4c",
		"originatorId": "SDNC_DG"
	  },
	  "payload": {
		"complex-composite-app-deploy-workflow-request": {
		  "composite-app-deploy-input-params": {
			"composite-application-parameters": {
			  "service-instance-name": "USER_INPUT_TARGET_CLUSTER_NAME",
			  "service-instance-description": "USER_INPUT_TARGET_CLUSTER_DESCRIPTION",
			  "service-instance-version": "USER_INPUT_TARGET_CLUSTER_VERSION",
			  "composite-app": "USER_INPUT_COMPOSITE_APP_NAME",
			  "composite-app-version": "USER_INPUT_COMPOSITE_APP_VERSION",
			  "composite-profile": "USER_INPUT_COMPOSITE_PROFILE"
			},
			"application-data-parameters": [{
			  "app-metadata-parameters": {
				"app-name": "USER_INPUT_APP_NAME"
			  },
			  "target-cluster-provider-parameters": [{
				"cluster-provider-name": "USER_INPUT_PROVIDER_NAME",
				"clusters": [{
				  "name": "USER_INPUT_CLUSTER_NAME",
				  "app-interfaces-parameters": [{
					"network-name": "USER_INPUT_NETWORK_NAME",
					"subnet-name": "USER_INPUT_SUBNET_NAME",
					"subnet-cidr-range": "USER_INPUT_SUBNET_CIDR_RANGE"
				  }]
				}]
			  }]
			}],
			"application-override-parameters": [{
			  "app-name": "USER_INPUT_APP_NAME",
			  "values": {
				"USER_INPUT_VARIABLE_NAME": "USER_INPUT_VARIABLE_VALUE"
			  }
			}]
		  },
		  "amcop-middle-end-properties": {
			"deploy-intent-group-api-url": "USER_INPUT_AMCOP_APP_API_URL",
			"amcop-middle-end-username": "",
			"amcop-middle-end-password": ""
		  },
		  "amcop-service-project-name": "USER_INPUT_AMCOP_SERVICE_PROJECT_NAME"
		}
	  }
	}


