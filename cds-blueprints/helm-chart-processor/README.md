# Step by step instructions to load this CBA

# Assumptions
  1. CDS up and running 
  
    :~$ kubectl get pods -n onap | grep cds
	dev-cds-blueprints-processor-b8656f584-wn6lh   1/1     Running    6          7d3h
	dev-cds-command-executor-78b98dff4f-8mtnk      1/1     Running    6          7d3h
	dev-cds-db-0                                   1/1     Running    6          7d3h
	dev-cds-py-executor-8499669b99-ph84w           1/1     Running    6          7d3h
	dev-cds-sdc-listener-749896d795-qgp2t          0/1     Init:0/1   350        7d3h
	dev-cds-ui-77f74ff6db-gbgwp                    1/1     Running    7          7d3h


  2. AMCOP 2.0 up and running. Note down emcoui IP and port no. as this need to be updated in 
     request payload 
	
	ubuntu@node1:~$ kubectl get svc -n onap4k8s
	NAME           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                           AGE
	cdap           NodePort    10.233.39.66    <none>        11011:31011/TCP,11015:31015/TCP   62d
	clm            ClusterIP   10.233.11.58    <none>        9061/TCP                          62d
	configsvc      NodePort    10.233.26.91    <none>        9082:30482/TCP                    62d
	dmaap          NodePort    10.233.8.195    <none>        3904:32392/TCP,3905:30768/TCP     62d
	**emcoui         NodePort    10.233.3.164    <none>        9080:30480/TCP                    62d**
	etcd           ClusterIP   10.233.21.72    <none>        2379/TCP,2380/TCP                 62d
	kafka1         ClusterIP   10.233.58.240   <none>        9092/TCP                          62d
	middleend      NodePort    10.233.37.191   <none>        9081:30481/TCP                    62d
	mongo          ClusterIP   10.233.8.236    <none>        27017/TCP                         62d
	ncm            ClusterIP   10.233.56.217   <none>        9031/TCP                          62d
	orchestrator   ClusterIP   10.233.41.158   <none>        9015/TCP                          62d
	ovnaction      ClusterIP   10.233.2.121    <none>        9053/TCP,9051/TCP                 62d
	rsync          ClusterIP   10.233.40.22    <none>        9041/TCP                          62d
	vescollector   NodePort    10.233.43.127   <none>        8080:31080/TCP                    62d
	zookeeper      ClusterIP   10.233.36.23    <none>        2181/TCP                          62d

  3. Assumption is that you had extracted or cloned the git repository under the $HOME folder

# Look into the data dictionary dd.json

	$ cd ~/equinix-pcei-poc/cds-blueprints/helm-chart-processor

	$ cat Scripts/dd.json

# Load the dd.json into CDS

	$ cd ../../util-scripts

	$ bash -x ./dd-microk8s.sh ../cds-blueprints/helm-chart-processor/Scripts/dd.json

# Create CBA Archive

	$ cd ../cds-blueprints/helm-chart-processor

	$ zip -r helm-chart-processor.zip *

# Enrich CBA Archive. It makes it portable to future CDS runtime releases
	
	$ cd ../../util-scripts

	$ bash -x ./enrich-and-download-cds-blueprint.sh ../cds-blueprints/helm-chart-processor/helm-chart-processor.zip

# Save/Deploy the CBA Archive into the CDS runtime (CDS desing time activity ends here)

	$ bash -x ./save-enriched-blueprint.sh /tmp/CBA/ENRICHED-CBA.zip

# Open a new SSH terminal to tail CDS blueprint processor logs

	$ cd ~/equinix-pcei-poc/util-scripts

	$ ./tail-cds-bp-log.sh

# Open a new SSH terminal to tail CDS py-executor log
	
	$ cd ~/equinix-pcei-poc/util-scripts

	$ bash -x ./tail-cds-py-executor-log.sh
	
# Update the Request JSON payload

	$ cd ~/equinix-pcei-poc/cds-blueprints/helm-chart-processor/Scripts

# Copy the template file
	$ cp request-payload.json.template request-payload.json
	
# Sample request payload
    # You can refer to the below EMCO REST API link to get more details about the JSON payload
	# https://wiki.onap.org/display/DW/V2+API+Specification
    # Note:- You should update the attribute values for USER_INPUT_XXX parameters
	# USER_INPUT_AMCOP_APP_API_URL  : here update the amcop installed machine ip with portno(30480).
    # ex : http://<amcop server ip>>:30480/middleend
    # USER_INPUT_CDS_PY_EXEC_POD_IP : Use below command to get the CDS pyexecutor IP
    # kubectl get pods -n onap -o wide | grep dev-cds-py-executor | awk '{print $6}'
    # USER_INPUT_GIT_URL - helm chart git repo url ex. "https://gitlab.com/api/v4"
    # USER_INPUT_GIT_PROJECT_ID - project id ex."26902714"
    # USER_INPUT_GIT_BRANCH - git branch name ex. "main"
	# USER_INPUT_GIT_ARCHIVE_FILE_NAME - any tar file name ex. "helm-charts-poc.tar.gz"
	# USER_INPUT_GIT_ACCESS_TOKEN - refer main readme to generate the git access token.
	# USER_INPUT_PROJECT_NAME - Enter the existing amcop project name.
	# USER_INPUT_SERVICE_NAME - Enter the service name ex:"PCEI-AZURE-IOT-SVC"

	
    $ vi request-payload.json
	{
	  "commonHeader": {
		"originatorId": "System",
		"requestId": "12345678",
		"subRequestId": "1234-122346"
	  },
	  "actionIdentifiers": {
		"mode": "sync",
		"blueprintName": "helm-chart-processor",
		"blueprintVersion": "9.0.1",
		"actionName": "helm-chart-prepare"
	  },
	  "payload": {
		"helm-chart-prepare-request": {
			"cds-py-exec-pod-ip": "USER_INPUT_CDS_PY_EXEC_POD_IP",
			"git-url": "USER_INPUT_GIT_URL",
			"git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
			"git-project-id": "USER_INPUT_GIT_PROJECT_ID",
			"git-branch-name": "USER_INPUT_GIT_BRANCH",
			"git-download-folder": "/opt/app/onap/python/test001",
			"git-archive-file-name": "USER_INPUT_GIT_ARCHIVE_FILE_NAME",
			"amcop-middle-end-properties": {
				"create-composit-app-api-url": "USER_INPUT_AMCOP_APP_API_URL",
				"amcop-middle-end-username": "",
				"amcop-middle-end-password": ""
			},
			 "amcop-service-information": {
                  "project-name": "USER_INPUT_PROJECT_NAME",
                  "service-name": "USER_INPUT_SERVICE_NAME",
                  "service-description": "USER_INPUT_SERVICE_DESCRIPTION"
              },
			"helm-charts-info": [{
				"helm-chart-folder": "USER_INPUT_HELM_CHART_FOLDER",
				"helm-chart-file-name": "USER_INPUT_HELM_CHART_FILE_NAME",
				"helm-values-yaml-override-properties": {
					"resources.small.limits.cpu": "USER_INPUT_RESOURCES_SMALL_LIMITES_CPU",
					"service.type": "USER_INPUT_SERVICE_TYPE",
					"service.internalPort": "USER_INPUT_SERVICE_INTERNALPORT"
				}
			}]
		}
      }
	}

# Update the request-payload.json
	$ cat request-payload.json
	$ export REQUEST_PAYLOAD="$PWD/request-payload.json"

# Get the CDS blueprint processor service IP
	$ CDS_BP_SVC_IP=$(kubectl get svc -n onap | grep 'cds-blueprints-processor-http' | awk '{print $3}')

	$ echo "CDS Blueprint processor IP = $CDS_BP_SVC_IP"

# Execute the below CURL Command to execute this blueprint
    # On success the below command will return HTTP status 200

	$ curl -v --location --request POST http://${CDS_BP_SVC_IP}:8080/api/v1/execution-service/process --header 'Content-Type: application/json;charset=UTF-8' --header 'Accept: application/json;charset=UTF-8,application/json' --header 'Authorization: Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==' --header 'Host: cds-blueprints-processor-http:8080' --header 'Content-Type: text/json' --data  "@$REQUEST_PAYLOAD" | python3 -m json.tool

# Now you can switch to the CDS blueprint processor log console for more information
	
# Look at the AMCOP 2.0 console UI to verify cluster provider displayed.

	http://AMCOP_PORTAL_IP:AMCOP_PORTAL_PORT_NUM/middleend/projects 





