#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
import logging
import os
import shutil
import yaml
import subprocess

from .helm_constants import HelmChartConstant

class HelmExecutor(object):
    """
    This class is mainly responsible to create the helm packages
    by using the helm commands.
    """

    def __init__(self, input_json, git_download_full_path):
        """
        Init method
        """

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("HelmExecutor init method called")
        self.input_json = input_json
        self.logger.info("helm input json :{}".format(input_json))
        self.git_download_full_path = git_download_full_path
        self.helm_chart_folder = None

        self.SERVICE_CREATE_PAYLOAD = {"name": "SVC_NAME",
                                       "description": "DESCRIPTION",
                                       "spec": {"projectName": "PROJECT_NAME",
                                       "appsData": []
                                      }
                                      }

    def command_executor(self, command_str):
        """
        Function is used to execute the given command
        Args:
            command_str(str) : command string which is going to execute as
                               one subprocess.
        Returns:
            process(obj): process object
        """
        override_file_name = HelmChartConstant.VALUE_YAML
        process = subprocess.Popen(command_str, stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE, shell=True,
                                   encoding="utf-8")
        return process

    def read_yaml_file(self, yaml_file_name):
        """
        This method will read and return the given YAML file
        contents.
        Args:
            yaml_file_name(str): file name
        Returns:
            data(string): yaml file data
        """
        self.logger.info("*************Read yaml file**************")
        with open(yaml_file_name) as file:
            data = yaml.load(file, Loader=yaml.FullLoader)
            self.logger.info("Yaml file data", data)
        return data

    def write_yaml_file(self, yaml_file_name, yaml_data):
        """
        This method is responsible to write input
        YAML data contents into the target file.
        Args:
            yaml_file_name(str): YAML file path
            yaml_data(str): YAML file data
        Returns: None
        """
        self.logger.info("*************Write to yaml file**************")
        self.logger.info("Yaml file name {}".format(yaml_file_name))
        with open(yaml_file_name, 'w') as file:
            yaml.dump(yaml_data, file)
        return

    def create_helm_chart(self, helm_folder_path, version):
        """
        This method is responsible to create the Helm package
        by executing helm command and returns the helm package
        TAR file path.
        Args:
            helm_folder_path(str): helm folder path
            version(string): helm package version
        Returns:
            helm_file_path(str): helm file path
        """
        self.logger.info("**********create_helm_chart**********")
        helm_folder_path, helm_base_folder = os.path.split(helm_folder_path)
        temp_folder_path = "{}/tmp/helm-package/{}".format(
                                                  self.git_download_full_path,
                                                  helm_base_folder)
        self.logger.info("helm folder path: {}".format(helm_folder_path))

        os.chdir(helm_folder_path)

        # Check the temp dir present if not create
        if not os.path.exists(temp_folder_path):
            os.makedirs(temp_folder_path)

        cmd_str = "helm package {} -d {}".format(helm_base_folder,
                                                 temp_folder_path)
        self.logger.info("Command String :{}".format(cmd_str))
        process = self.command_executor(cmd_str)
        stdout, stderr = process.communicate()
        # Helm package will like this {$helm_base_folder}-0.1.0.tgz
        helm_file_path = "{}/{}-{}.tgz".format(temp_folder_path,
                                               helm_base_folder,
                                               version)
        self.logger.info("The output of the helm command: {}".format(stdout))
        self.logger.info("Command error: {}".format(stderr))
        self.logger.info("Helm package file path :{}".format(helm_file_path))

        return helm_file_path

    def prepare_emco_override_profile_tar(self, helm_folder_path,
                                          override_input_data_dict):
        """
        This method is responsible to create the EMCO helm package
        override package. This is required for EMCO while onboarding the
        an application.
        Args:
            helm_folder_path(str): helm dir path
            override_input_data_dict(dict): user given override key, value pair
        Returns:
            tar_file_name(str): full path of the tar file
        """
        self.logger.info("*********prepare_emco_override_profile_tar*******")
        helm_base_folder = os.path.basename(helm_folder_path)
        temp_folder_path = "{}/tmp/helm-package/{}/profile".format(
                                                   self.git_download_full_path,
                                                   helm_base_folder)

        # Check the temp dir present if not create
        if not os.path.exists(temp_folder_path):
            os.makedirs(temp_folder_path)

        dst_file_path = self.override_values_yaml(helm_folder_path, 
                                                  temp_folder_path)
        self.override_manifest_yaml(temp_folder_path)

        # Read the values.yaml file
        yaml_data = self.read_yaml_file(dst_file_path)
        self.logger.info("Yaml_data from values yaml file:  {}".format(yaml_data))
        self.override_helm(override_input_data_dict, yaml_data, dst_file_path)

        os.chdir(temp_folder_path)
        # Helm package will like this {$helm_base_folder}-0.1.0.tgz
        tar_file_name = "{}/{}-profile.tar.gz".format(temp_folder_path,
                                                      helm_base_folder)
        cmd_str = "tar -cvzf {} *".format(tar_file_name)
        process = self.command_executor(cmd_str)
        stdout, stderr = process.communicate()

        self.logger.info("The output of the command: {}".format(stdout))
        self.logger.info("Command error: {}".format(stderr))
        self.logger.info("Tar file name with full path:{}".format(tar_file_name))

        return tar_file_name

    def override_values_yaml(self, helm_folder_path, temp_folder_path):
        """
        This function is responsible to override the values.yaml file
        """
        # Copy the Value.yaml file
        src_file_path = "{}/{}".format(helm_folder_path,
                                       HelmChartConstant.VALUE_YAML)
        dst_file_path = "{}/{}".format(temp_folder_path,
                                       HelmChartConstant.OVER_RIDE_VALUES)
        self.logger.info("Source YAML file path :{}".format(src_file_path))
        self.logger.info("Destination YAML file path :{}".format(dst_file_path))
        shutil.copy2(src_file_path, dst_file_path)

        return dst_file_path

    def override_manifest_yaml(self, temp_folder_path):
        """
        This function is responsible to override the manifest.yaml file
        """
        # Copy the manifest.yaml file
        script_dir = os.path.abspath(os.path.join(__file__, "../../.."))
        src_manifiest_file_path = "{}/Templates/{}".format(script_dir,
                                               HelmChartConstant.MANIFEST_FILE)
        dst_manifiest_file_path = "{}/{}".format(temp_folder_path,
                                                 HelmChartConstant.MANIFEST_FILE)
        self.logger.info("Source YAML manifest :{}".format(
                                                      src_manifiest_file_path))
        self.logger.info("Destination YAML manifest :{}".format(
                                                      dst_manifiest_file_path))

        shutil.copy2(src_manifiest_file_path, dst_manifiest_file_path)

    def override_helm(self, override_input_data_dict, yaml_data, dst_file_path):
        """
        This function is used to override the helm file
        """
        # Over ride the values.yaml form the override_input_data_dict
        # Here the logic to override the helm file
        for dt_key in override_input_data_dict:
            # print(user_data[dt_key])
            key_lst = dt_key.split('.')
            update_data = yaml_data
            while len(key_lst) > 1:
                val = key_lst.pop(0)
                update_data = update_data[val]
                # print("modifiy data in loop",update_data )
                if len(key_lst) == 1:
                    # print(user_data[dt_key])
                    update_data[key_lst[0]] = override_input_data_dict[dt_key]

        self.logger.info("Updated Yaml Data:{}".format(yaml_data))
        self.write_yaml_file(dst_file_path, yaml_data)

    def process_helm_charts(self):
        """
        This method is responsible to process a helm chart creation, 
        override helm chart from the input json
        :return: It will return the list of helm chart
        """
        self.logger.info("******* Process the chart method *******")
        helm_chart_lst = []
        self.logger.info("Git Download the path{}".format(
                                                  self.git_download_full_path))
        helm_chart_info_lst = self.input_json["helm-charts-info"]
        self.logger.info("helm chart info from the payload:{}".format(
                                                          helm_chart_info_lst))

        # Caller to create the service payload json
        amcop_service_payload = self.create_service_payload()

        onboard_app_data = {}
        file_inc = 0
        for helm_chart in helm_chart_info_lst:

            self.logger.info("helm_chart from the payload:{}".format(
                helm_chart))
            override_value = helm_chart["helm-values-yaml-override-properties"]
            helm_folder_path = os.path.join(self.git_download_full_path,
                                            helm_chart["helm-chart-folder"])

            helm_package_version = helm_chart["helm-chart-version"]
            # helm_package_tar file full path will be stored in this variable
            helm_package_tar = self.create_helm_chart(helm_folder_path,
                                                      helm_package_version)
            self.logger.info("helm_package_tar :{}".format(helm_package_tar))

            # prepare helm override profile tar
            helm_override_tar = self.prepare_emco_override_profile_tar(
                                                              helm_folder_path,
                                                              override_value)
            self.logger.info("helm_override_tar :{}".format(helm_override_tar))

            helm_package_tar_base_name = os.path.basename(helm_package_tar)
            append_helm_package = ('file_{}'.format(file_inc),
                                   (helm_package_tar_base_name,
                                    open(helm_package_tar, 'rb'),
                                    'application/octet-stream'))

            helm_override_tar_base_name = os.path.basename(helm_override_tar)
            self.logger.info("File increment :{}".format(file_inc))

            file_inc += 1
            append_helm_override_tar = ('file_{}'.format(file_inc),
                                        (helm_override_tar_base_name,
                                         open(helm_override_tar, 'rb'),
                                         'application/octet-stream'))
            file_inc += 1

            self.logger.info("File increment :{}".format(file_inc))

            helm_chart_lst.append(append_helm_package)
            helm_chart_lst.append(append_helm_override_tar)
            onboard_app_data = self.create_onboard_app_data(helm_chart,
                                       helm_package_tar_base_name,
                                       helm_override_tar_base_name)

            amcop_service_payload["spec"]["appsData"].append(onboard_app_data)

        # This is out side the for loop
        self.logger.info("The helm chart list:{}".format(helm_chart_lst))
        self.logger.info("Service payload {}".format(amcop_service_payload))
        return amcop_service_payload, helm_chart_lst

    def create_onboard_app_data(self, helm_chart, helm_package_tar_base_name,
                               helm_override_tar_base_name):
        """
        This function is responsible to create the onboard app data
        """
        TEMP_APP_ONBOARD_PAYLOAD = {"metadata": {"name": "APP_NAME",
                                    "description": "APP_DESCRIPTION",
                                    "filename": "APP_HELM_PACKAGE_FILE_NAME"
                                    },
                                    "profileMetadata": {
                                     "name": "APP_HELM_OVERRIDE_PROFILE_NAME",
                                     "filename": "APP_HELM_OVERRIDE_FILE_NAME"
                                    }
                                    }

        onboard_app_data = TEMP_APP_ONBOARD_PAYLOAD
        #app_name = "{}.1.0".format(helm_package_tar_base_name.split(".")[0])
        app_name = helm_chart["helm-chart-folder"]
        self.logger.info("Application Name :{}".format(app_name))
        onboard_app_data["metadata"]["name"] = app_name
        onboard_app_data["metadata"]["description"] = \
                                                "desc of {}".format(app_name)
        onboard_app_data["metadata"]["filename"] = helm_package_tar_base_name
        onboard_app_data["profileMetadata"]["name"] = \
                                    helm_override_tar_base_name.split(".")[0]
        onboard_app_data["profileMetadata"]["filename"] = \
                                                    helm_override_tar_base_name
        return onboard_app_data

    def create_service_payload(self):
        """
        This method to

        :return:
        """
        self.logger.info("*******create_service_payload**********")

        # Extract the data from the input json
        amcop_info = self.input_json.get("amcop-service-information")
        project_name = amcop_info.get("project-name")
        service_name = amcop_info.get("service-name")
        service_des = amcop_info.get("service-description")

        # Service payload from the constant
        service_payload = self.SERVICE_CREATE_PAYLOAD
        self.logger.info("before service layload {}".format(service_payload))
        service_payload["name"] = service_name
        service_payload["description"] = service_des
        service_payload["spec"]["projectName"] = project_name

        self.logger.info("service  payload information {}".format(service_payload))

        return service_payload
