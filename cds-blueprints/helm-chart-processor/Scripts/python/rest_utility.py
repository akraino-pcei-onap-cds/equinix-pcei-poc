#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
import logging
import requests

class RestUtility(object):
    """
    This class is responsible to handle all the GET and POST 
    requests using requests library.
    """

    def __init__(self):
        """
        Init method
        """

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("RestUtility init method called")

    def fetch_details(self, url, payload):
        """
        rest get operation utility method
        Args:
             url: url
             payload: str
        Returns:
            response(dict): json response
        """
        self.logger.info("url: {}".format(url))
        self.logger.info("payload {}", payload)
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.request("GET", url, headers=headers)
        self.logger.info("response_data", response)
        return response


    def upload_post(self, url, payload, file_list):
        """
        Function is responsible to download the file from git repository and
        extract it in local.
        Args:
             url: url
             file_list: files list
             payload: str
        Returns:
            response(dict): json response
        """
        self.logger.info("url: {}".format(url))
        self.logger.info("url: {}".format(file_list))

        self.logger.info("upload url: %s", url)

        # upload the service files
        response = requests.post(url, data=payload, files=file_list)
        return response
