from RestUtils import *
from utility import *
import os
import json
print(os.path.abspath(os.path.join(__file__)))
print(os.path.abspath(os.path.join(__file__,"../../..")))


input_json = {"cds-py-exec-pod-ip": "10.1.1.12",
              "git-download-folder": "/mnt/c/Users/Aarna/Desktop/aarna/CDS/cba-sandbox/test_helm",
              "git-url": "https://gitlab.com/api/v4",
              "git-archive-file-name": "helm_nrm.tar.gz",
              "git-access-token": "u2yfqVgTRqozEhEPXq6d",
              "git-branch": "master",
              "git-project-id": "26871358",
              "helm-charts-info": [
                  {"helm-values-yaml-override-properties": {
                      "resources.small.limits.cpu": "100m", "service.internalPort": "830",
                      "service.type": "NodePort"
                  },
                      "helm-chart-folder": "nrm-simulator-3gpp-mns"
                  }
              ],
              "amcop-middle-end-properties":
                  {
                   "create-composit-app-api-url": "http://192.168.100.24:30480/middleend",
                   "amcop-middle-end-password": "",
                   "amcop-middle-end-username": ""
                  },
              "amcop-service-information": {
                  "project-name": "TestProjectOne",
                  "service-name": "nrm_service",
                  "service-description": "Nrm service description"
              }
              }

sub_request_id = '1234567'

# download_resp = GitDownloadUtility().download_file(input_json)
# tar_extract_resp = TarArchiveUtility().create_and_extract_tar(input_json,
#                                  download_resp["response_data"].content, sub_request_id)
# print("Response data",tar_extract_resp["response_data"])
# print("Extracte full path", tar_extract_resp["extracted_full_path"])
extracted_folder_name = "helm_nrm"
extracted_full_path = "/mnt/c/Users/Aarna/Desktop/aarna/CDS/cba-sandbox/test_helm/1234567/helm_nrm"
git_full_path = extracted_full_path
helm_obj = HelmExecutor(input_json, git_full_path)
# helm_obj.helm_override_file()
amcop_service_payload, helm_chart_lst = helm_obj.process_helm_charts()

project_name = input_json["amcop-service-information"]["project-name"]

payload = {'servicePayload': json.dumps(amcop_service_payload)}

base_url = input_json["amcop-middle-end-properties"]["create-composit-app-api-url"]
post_url = "{}/projects/{}/composite-apps".format(base_url, project_name)
print("*********************************")
print("the payload : ", payload)
print("The helm chart list:", helm_chart_lst)
print("post url", post_url)
print("REST API Call")
response = RestUtility().upload_post(post_url, payload, helm_chart_lst)
print(response.status_code)
print(response.text)
# helm_obj.create_service_payload()