#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
import logging
import requests

class GitDownloadUtility(object):
    """
    This class is responsible to download the files from
    GIT Repository.
    """

    def __init__(self):
        """
        Init method
        """

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("GitDownloadUtility init method called")


    def download_file(self, input_json):
        """
        Function is responsible to download the file from git repository and
        extract it in local.
        Args:
            input_json(dict): input fields
        Returns:
            response(dict): response of git download
        """
        self.logger.info("input json: {}".format(input_json))
        headers = {
            'Content-Type': 'application/octet-stream',
            'PRIVATE-TOKEN': input_json["git-access-token"]
        }
        url = "{}/projects/{}/repository/archive".format(input_json["git-url"],
                                                         input_json["git-project-id"])
        self.logger.info("git repository archive url: %s" % (url))

        # download git repository
        response = requests.get(url, headers=headers,
                                params=(('sha', input_json["git-branch"]),))
        status_code = response.status_code
        self.logger.info("Response code: {}".format(status_code))
        if status_code == 200:
            self.logger.info("Successfully downloaded from Git Repository")
            status = "Success"
            resp_data = response
        elif status_code == 404:
            self.logger.info("Invalid Project id or branch name")
            status = "Failure"
            resp_data = "Invalid Project id or branch name"
        elif status_code == 401:
            self.logger.info("Provided access token is invalid")
            status = "Failure"
            resp_data = "Provided access token is invalid"
        else:
            self.logger.info("Failed to download the file from GIT")
            status = "Failure"
            resp_data = "Failed to download the file from GIT"

        return {"Status": status, "response_code": status_code, 
                "response_data": resp_data}
