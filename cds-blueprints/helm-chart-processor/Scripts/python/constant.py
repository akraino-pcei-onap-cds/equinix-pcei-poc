VALUE_YAML = "values.yaml"
OVER_RIDE_VALUES = "override_values.yaml"
MANIFEST_FILE = "manifest.yaml"
SERVICE_CREATE_PAYLOAD = {"name": "SVC_NAME", "description": "DESCRIPTION",
                          "spec": {"projectName": "PROJECT_NAME",
                                   "appsData": []
                                   }
                          }

APP_ONBOARD_PAYLOAD = {"metadata": {"name": "APP_NAME",
                                    "description": "APP_DESCRIPTION",
                                    "filename": "APP_HELM_PACKAGE_FILE_NAME"
                                    },
                       "profileMetadata": {
                            "name": "APP_HELM_OVERRIDE_PROFILE_NAME",
                            "filename": "APP_HELM_OVERRIDE_FILE_NAME"
                        }
                       }
