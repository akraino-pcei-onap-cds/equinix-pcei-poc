#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import json
import logging
from blueprints_grpc import executor_utils
from blueprints_grpc.blueprint_processing_server import AbstractScriptFunction
from google.protobuf import json_format
from .git_download_utility import GitDownloadUtility
from .tar_archive_utility import TarArchiveUtility
from .helm_exec_utility import HelmExecutor
from .rest_utility import RestUtility


class GenericHelmChartProcessor(AbstractScriptFunction):
    """
    This class is reponsible to create the helm packages
    using the helm commands.
    """

    def __init__(self):
        """
        Init method
        """

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("GenericHelmChartProcessor init method called")

    def process(self, execution_request):
        """
        This method will be executed by the CDS py-executor micro service
        over GRPC call coming from CDS blueprint processor to the py-executor
        micro service.

        Parameters
        ----------
        execution_request : Dictionary
            It has all attributes sent from NB API payload to the
                        CDS blue print processor.
        """

        self.logger.info("Request Received in Script : {}".format(
                                                             execution_request))
        common_data = json.loads(json_format.MessageToJson(
                                               execution_request.commonHeader))
        sub_request_id = common_data.get("subRequestId")
        input_json = json.loads(json_format.MessageToJson(
                                            execution_request.payload)) \
                                           ["GenericHelmChartProcessor-request"]
        self.logger.info("helm input json :{}".format(input_json))

        execute_rest_api = {"response_code": 500, "response_data": "Internal Error"}

        result = "success"
        download_resp = GitDownloadUtility().download_file(input_json)
        if download_resp["response_code"] == 200:
            tar_extract_resp = TarArchiveUtility().create_and_extract_tar(
                                         input_json,
                                         download_resp["response_data"].content, 
                                         sub_request_id)

            if tar_extract_resp["response_code"] == 200:
                extracted_full_path = tar_extract_resp["extracted_full_path"]
                amcop_service_payload, helm_chart_lst = \
                                               self.helm_processor(input_json, 
                                                            extracted_full_path)

                # Execute the Rest api call
                execute_rest_api = self.create_composite_apps(input_json, 
                                                    amcop_service_payload, 
                                                    helm_chart_lst)

                if execute_rest_api["response_code"] != 201:
                    execution_response = executor_utils.failure_response(
                                            execution_request,
                                            execute_rest_api["response_data"],
                                            execute_rest_api["response_code"],
                                            "AMCOP add Service command "
                                            "execution failure")
                    result = "Failure"
            else:
                self.logger.info("Failed to extract the tar file")
                execution_response = executor_utils.failure_response(
                                            execution_request,
                                            tar_extract_resp["response_data"],
                                            tar_extract_resp["response_code"],
                                            "Tar Extraction failure")
                result = "Failure"

        else:
            self.logger.info("Failed to download the file from git repository")
            execution_response = executor_utils.failure_response(
                                              execution_request,
                                              download_resp["response_data"],
                                              download_resp["response_code"],
                                              "Failed to download from GIT")
            result = "Failure"

        # This is to send the response back to the NB API call
        # In general use the HTTP status code
        self.logger.info("response: {}".format(execute_rest_api["response_data"]))

        if result == "success":
            execution_response = executor_utils.success_response(
                                            execution_request,
                                            execute_rest_api["response_data"],
                                            execute_rest_api["response_code"])
        # Log the response
        self.logger.info("Response returned : {}".format(execution_response))

        # Yeild the execution reponse and the py-exectuor framework will
        # include it in the NB response JSON.
        yield execution_response

    def recover(self, runtime_exception, execution_request):
        """
        Method will get invoked incase the process method fails with
                some exception.
        """
        return None

    def send_notification(self, execution_request):
        """
        Method is to send notifications. Just send simple response message.
        """
        yield executor_utils.send_notification(execution_request, 
                                               "I am notification")

    def helm_processor(self, input_json, extracted_full_path):
        """
        Function is used to handle the helm charts such as 
        creation and override the charts from the input json.
        Args:
            input_json(dict): input fields
            extracted_full_path(str):
        Returns:
            response(dict): response of the helm charts.
        """
        helm_obj = HelmExecutor(input_json, extracted_full_path)
        return helm_obj.process_helm_charts()

    def create_composite_apps(self, input_json, amcop_service_payload, 
                              helm_chart_lst):
        """
        This function is used to create the composite applications
        Args:
            input_json(dict): required input fields
            amcop_service_payload(dict): service payload
            helm_chart_lst(list): helm chart list
        Returns:
            response(dict): response of the composite app.
        """
        # Get the project name form the input json
        project_name = input_json["amcop-service-information"]["project-name"]
        payload = {'servicePayload': json.dumps(amcop_service_payload)}
        base_url = input_json["amcop-middle-end-properties"] \
                                   ["create-composit-app-api-url"]
        post_url = "{}/projects/{}/composite-apps".format(base_url, project_name)

        response = RestUtility().upload_post(post_url, payload, helm_chart_lst)
        self.logger.info(response.status_code)
        self.logger.info(response.text)
        response = {"response_code": response.status_code, 
                    "response_data": response.text}
        return response
