import requests
import json
import subprocess
from RestUtils import *

urlget = "http://192.168.100.24:30480/v2/projects"
response = RestUtility().fetch_details(urlget, "")
print(response.text)

payload={'servicePayload': '{"name":"vfirewall","description":"vfw service","spec":{"projectName":"TestProjectOne","appsData":[{"metadata":{"name":"sink","description":"sink application","filename":"sink.tgz"},"profileMetadata":{"name":"sink_profile","filename":"sink_profile.tar.gz"},"blueprintModels":[]}]}}'}
files=[
    ('file',('sink.tgz',
             open('/home/kavitha/project/workspace/Kavitha_Work/CBAHelm/helm-testing/samples/sink.tgz','rb'),
             'application/octet-stream')),
    ('file1',('sink_profile.tar.gz',
              open('/home/kavitha/project/workspace/Kavitha_Work/CBAHelm/helm-testing/samples/sink_profile.tar.gz','rb'),
              'application/octet-stream'))
]
posturl = "http://192.168.100.24:30480/middleend/projects/TestProjectOne/composite-apps"
response = RestUtility().upload_post(posturl, payload, files)

print(response.text)