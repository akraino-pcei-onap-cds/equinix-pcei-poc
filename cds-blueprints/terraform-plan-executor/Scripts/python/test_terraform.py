from utility import *

input_json = {"terraform-workspace-name": "Workspace",
              "terraform-action": "destroy",
              "git-archive-file-name": "terraform-test.tar.gz",
              "git-project-id": "26599931",
              "terraform-variable-override": {"location": "westus",
                                              "resource_group_name": "sandbox-vms",
                                              "access_token": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+qJkJfLzzp9zyKvrc+9D4lSSSP6BqHp6eLuQHEZ60LspZAtjtDvRfGn9JQZTiGcIaNz8vBfyDrNbiED6Pbrlq88riDZl5a8uLqMx3LAp5jJMpcMioljEAwf506QmFC8FJGmcZJaFQ3RCS+5jBCybBMKI8Y64Yw0IYGua38Ws1QRGLYhgbfKnMzdDF5jT13LScbcAeGAd137Ffo2E56x8usJhmG46mIzt80hvBXf3oCogAGDEGVvtXKk7PiUh0QfJ6mtKX7+vPwnacl7dCdTjyeyt6X23DyNYITePcKJ0TneUUFDV3FcwFheadewekJ7cTIvs/Xu07VQ98JtM/Gn53",
                                              "virtual_network_cidr": ["10.0.0.0/16"],
                                              "virtual_subnet_cidr": ["10.0.1.0/24"],
                                              "serveruserName": "ubuntu"},
              "terraform-environment-variables": {
                  "ARM_ACCESS_KEY": "i4zYjECZd+ZOtIO4/49XpLIcipvZ4fyxzhJWIo70qAhCUkiz6/4Gwh8UbVcdgeOgrowQAlN3AC1TzMh0ruRujQ==",
                  "ARM_CLIENT_ID": "b9e4b57c-e473-4e94-80c3-0d36c34dbe9c",
                  "ARM_TENANT_ID": "73dd1b1d-50e1-48d5-bb07-ec117d4a66a1",
                  "ARM_SUBSCRIPTION_ID": "7c364523-89cf-451a-91d8-3a1a71dcb424",
                  "ARM_CLIENT_SECRET": "Zt.-iXZI_PjEi80fVHT2Io1J.y~44T_2Kl"},
              "git-download-folder": "/opt/app/onap/blueprints/deploy",
              "cds-py-exec-pod-ip": "10.1.1.12",
              "git-url": "https://gitlab.com/api/v4",
              "terraform-var-file-name": "testenv.tfvars",
              "terraform-plan-folder": "/mnt/c/Users/Aarna/Desktop/aarna/CDS/Terraform/azure-infrastructure/create-vm",
              "git-branch": "development",
              "git-access-token": "pkhmvHuLX5pQEKLM_HWz"}


def tf_command_executor(input_json):
    """
    Function is used to execute the terraform actions
    Args:
        input_json(dict): input fields
    Returns:
        response(dict): response of the terraform actions.
    """
    tf_exec_utility = TerraformCommandExecutor(input_json)
    tf_exec_util_resp = tf_exec_utility.tf_override_file(input_json)
    if tf_exec_util_resp["response_code"] == 200:
        tf_default_action = {"init": tf_exec_utility.init_plan,
                             "workspace": tf_exec_utility.create_workspace,
                             "plan": tf_exec_utility.validate_plan,
                             "apply": tf_exec_utility.apply_plan,
                             "destroy": tf_exec_utility.destroy_plan}
        response = {"response_data": {}}

        for tf_plan in tf_default_action:
            if tf_plan in ["init", "workspace", "plan"] or \
                    input_json["terraform-action"] == tf_plan:
                print("tf_plan: {}".format(tf_plan))
                action_response = tf_default_action[tf_plan]()
                tf_action = "terraform" + tf_plan
                if action_response["response_code"] == 200:
                    response["response_data"][tf_action + "-status"] = \
                        {"response_code": str(action_response["response_code"]),
                         "stdout": action_response["stdout"],
                         "stderror": action_response["stderr"]}
                    response["response_code"] = 200
                else:
                    response["response_data"][tf_action + "-status"] = {
                        "response_code": str(action_response["response_code"]),
                        "stdout": action_response["stdout"],
                        "stderror": action_response["stderr"]}
                    response["response_code"] = 500
                    return response
    else:
        return tf_exec_util_resp

    return response


out_put = tf_command_executor(input_json)
print(out_put)
