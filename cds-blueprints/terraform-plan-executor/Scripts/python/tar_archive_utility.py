#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

import logging
import os
import tarfile

class TarArchiveUtility(object):
    """
    This class is responsible to create and extract the tar archive .
    """
    def __init__(self):
        """
        Init method
        """

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("TarArchiveUtility init method called")

    def create_and_extract_tar(self, input_json, download_content, request_id):
        """
        Function is used to create tar file and extract it
        in the same location
        Args:
            input_json(dict): input fields
            download_content(bytes): download response content
            request_id(str): request id
        Returns:
            response(dict): response of request

        """
        try:
            git_download_loc = input_json["git-download-folder"]
            archive_file = input_json["git-archive-file-name"]
            download_location = os.path.join(git_download_loc, request_id)
            self.logger.info("download location: %s" %(download_location))
            output_tar_file = os.path.join(download_location, archive_file)
            self.logger.info("tar file location: %s" %(output_tar_file))
            extract_dir = archive_file.split(".")[0]
            # creates a directory if not exists
            os.makedirs(download_location)
            os.chdir(download_location)
            with open(output_tar_file, "wb") as out:
                out.write(download_content)

            my_tar = tarfile.open(output_tar_file)
            my_tar.extractall(download_location)
            my_tar.close()

            extract_folder = [ fname for fname in os.listdir(download_location)
                    if os.path.isdir(os.path.join(download_location, fname)) ][0]
            os.rename(extract_folder, extract_dir)
            os.chdir(extract_dir)

            self.logger.info("extracted root dir name: {}".format(
                                os.path.join(download_location, extract_dir)))

            return {"Status": "Success", "response_code": 200,
                    "response_data": "{}".format(extract_dir)}

        except Exception as extract_err:
            return {"Status": "Failure", "response_code": 500,
                    "response_data": str(extract_err)}
