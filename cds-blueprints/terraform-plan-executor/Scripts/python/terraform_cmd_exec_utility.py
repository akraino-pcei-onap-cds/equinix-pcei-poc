#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

import logging
import os
import subprocess
import json

class TerraformCommandExecutor(object):
    """
    This class is mainly responsible to execute the terraform plans
    """
    def __init__(self, input_json):
        """
        Init method
        """

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("TerraformCommandExecutor init method called")
        self.input_json = input_json
        self.terraform_plan_folder = None

    def command_executor(self, command_str):
        """
        Function is used to execute the given command
        :param command_str: Command string need to pass an argument
        :return:
        """
        terraform_env_var = self.input_json["terraform-environment-variables"]
        for k, v in terraform_env_var.items():
            os.environ[k] = v

        process = subprocess.Popen(command_str, stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE, shell=True, 
                                   encoding="utf-8", env=os.environ)
        return process

    def tf_override_file(self):
        """
        Function is responsible to create terraform override file
        """
        try:
            self.logger.info("****Tf_override_file creation *******")

            terraform_var = self.input_json["terraform-variable-override"]
            override_str = []

            for k, v in terraform_var.items():
                if type(v) == str:
                    override_str.append(k + "\t=\t" + "\"{}\"".format(v))
                else:
                    override_str.append(k + "\t=\t" + json.dumps(v))

            self.logger.debug("Override string {}".format(override_str))
            if not os.path.isdir(self.input_json["terraform-plan-folder"]):
                os.makedirs(self.input_json["terraform-plan-folder"])

            os.chdir(self.input_json["terraform-plan-folder"])
            vars_file = os.path.join(os.getcwd(), 
                                     self.input_json["terraform-var-file-name"])

            # Terraform folder path
            self.terraform_plan_folder = os.path.dirname(vars_file)
            tf_var_data = "\n".join(override_str)
            self.logger.info("File path :{}".format(vars_file))
            with open(vars_file, "w") as tf_var:
                tf_var.write(tf_var_data)
            return {"Status": "Success", "response_code": 200,
                    "response_data": "Creation of tf var file completed "
                                     "Successfully"}
        except Exception as tf_override_err:
            self.logger.error("Got an Error {}".format(tf_override_err))
            return {"Status": "Failure", "response_code": 500,
                    "response_data": str(tf_override_err)}

    def init_plan(self):
        """
        terraform init plan
        """
        self.logger.info("******Going execute the init plan*****")
        terraform_env_var = self.input_json["terraform-environment-variables"]
        self.logger.info("Before executing the init plan:{}".format(
                                                            terraform_env_var))
        self.logger.info("Terraform plan folder:{}".format(
                                                self.terraform_plan_folder))
        os.chdir(self.terraform_plan_folder)
        self.logger.info("After the changing the dir{}".format(os.getcwd()))

        cmd_str = "terraform init"
        process = self.command_executor(cmd_str)
        stdout, stderr = process.communicate()
        self.logger.info("The output of the terraform init: {}".format(stdout))
        self.logger.info("Command error: {}".format(stderr))
        status_code = 200 if not process.returncode else 500
        return {"response_code": status_code, "stdout": stdout,
                "stderr": stderr}

    def create_workspace(self):
        """
        terraform create workspace
        """
        self.logger.info("*******Terraform create workspace method**********")
        tf_wrk_name = self.input_json["terraform-workspace-name"]
        cmd_str = "terraform workspace new {}".format(tf_wrk_name)
        process = self.command_executor(cmd_str)
        stdout, stderr = process.communicate()
        self.logger.info("The output of the create Workspace: {}".format(stdout))
        self.logger.info("Command error: {}".format(stderr))

        if "already exists" in stderr:
            cmd_str = "terraform workspace select {}".format(tf_wrk_name)
            process = self.command_executor(cmd_str)
            stdout, stderr = process.communicate()
            self.logger.info("The output of the create Workspace: {}".format(stdout))
            self.logger.info("Command error: {}".format(stderr))
            status_code = 200 if not process.returncode else 500
        else:
            status_code = 200 if not process.returncode else 500

        return {"response_code": status_code, "stdout": stdout,
                "stderr": stderr}

    def validate_plan(self):
        """
        terraform plan validate
        """
        self.logger.info("***********Terraform plan validate method**************")
        tf_vars = self.input_json["terraform-var-file-name"]
        self.logger.info("Terraform var file name :{}".format(tf_vars))

        cmd_str = "terraform plan -var-file={}".format(tf_vars)
        process = self.command_executor(cmd_str)
        stdout, stderr = process.communicate()
        self.logger.info("The output of the terraform plan validate: {}".format(
                                                                    stdout))
        self.logger.info("Command error: {}".format(stderr))
        status_code = 200 if not process.returncode else 500
        return {"response_code": status_code, "stdout": stdout,
                "stderr": stderr}

    def apply_plan(self):
        """
        terraform apply plan
        """
        self.logger.info("************Apply plan method***********")
        tf_vars = self.input_json["terraform-var-file-name"]
        self.logger.info("Terraform var file name :{}".format(tf_vars))

        cmd_str = "terraform apply -var-file={} -auto-approve".format(tf_vars)
        process = self.command_executor(cmd_str)
        stdout, stderr = process.communicate()
        self.logger.info("The output of the terraform apply plan: {}".format(
                                                                     stdout))
        self.logger.info("Command error: {}".format(stderr))
        status_code = 200 if not process.returncode else 500
        return {"response_code": status_code, "stdout": stdout,
                "stderr": stderr}

    def destroy_plan(self):
        """
        terraform destroy plan
        """
        self.logger.info("**********Destroy plan method*************")
        tf_vars = self.input_json["terraform-var-file-name"]
        self.logger.info("Terraform var file name :{}".format(tf_vars))

        cmd_str = "terraform destroy -var-file={} -auto-approve".format(tf_vars)
        process = self.command_executor(cmd_str)
        stdout, stderr = process.communicate()
        self.logger.info("The output of the terraform destroy plan: {}".format(
                                                                    stdout))
        self.logger.info("Command error: {}".format(stderr))
        status_code = 200 if not process.returncode else 500
        return {"response_code": status_code, "stdout": stdout,
                "stderr": stderr}
