#!/usr/bin/python
#
#  Copyright © 2023 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 4.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE0.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import json
import logging
from blueprints_grpc import executor_utils
from blueprints_grpc.blueprint_processing_server import AbstractScriptFunction
from google.protobuf import json_format
from .git_download_utility import GitDownloadUtility
from .tar_archive_utility import TarArchiveUtility
from .terraform_cmd_exec_utility import TerraformCommandExecutor


class GenericTerraformPlanExecutor(AbstractScriptFunction):
    """
    This class is reposible to do the terraform plan execution.
    """

    def __init__(self):
        """
        Init method
        """

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("GenericTerraformPlanExecutor init method called")

    def process(self, execution_request):
        """
        This method will be executed by the CDS py-executor micro service
        over GRPC call coming from CDS blueprint processor to the py-executor
        micro service.

        Parameters
        ----------
        execution_request : Dictionary
            It has all attributes sent from NB API payload to the
                        CDS blue print processor.
        """

        self.logger.info("Request Received in Script : {}".format(
                                                           execution_request))
        common_data = json.loads(json_format.MessageToJson(
                                               execution_request.commonHeader))
        sub_request_id = common_data.get("subRequestId")
        input_json = json.loads(json_format.MessageToJson(
            execution_request.payload))["GenericTerraformPlanExecutor-request"]
        download_resp = GitDownloadUtility().download_file(input_json)
        if download_resp["response_code"] == 200:
            tar_extract_resp = TarArchiveUtility().create_and_extract_tar(
                                 input_json,
                                 download_resp["response_data"].content,
                                 sub_request_id)

            result = "success"
            if tar_extract_resp["response_code"] == 200:
                tf_cmd_exec_resp = self.tf_command_executor(input_json)
                if tf_cmd_exec_resp["response_code"] != 200:
                    execution_response = executor_utils.failure_response(
                                            execution_request,
                                            tf_cmd_exec_resp["response_data"],
                                            tf_cmd_exec_resp["response_code"],
                                        "terraform command execution failure")
                    result = "Failure"
            else:
                self.logger.info("Failed to extract the tar file")
                execution_response = executor_utils.failure_response(
                                            execution_request,
                                            tar_extract_resp["response_data"],
                                            tar_extract_resp["response_code"],
                                            "Tar Extraction failure")
                result = "Failure"
        else:
            self.logger.info("Failed to download the file from git repository")
            execution_response = executor_utils.failure_response(
                                              execution_request,
                                              download_resp["response_data"],
                                              download_resp["response_code"],
                                              "Failed to download from GIT")
            result = "Failure"

        # This is to send the response back to the NB API call
        # In general using the HTTP status code
        self.logger.info("response: {}".format(
                                           tf_cmd_exec_resp["response_data"]))
        if result == "success":
            execution_response = executor_utils.success_response(
                                            execution_request,
                                            tf_cmd_exec_resp["response_data"],
                                            tf_cmd_exec_resp["response_code"])
        # Log the response
        self.logger.info("Response returned : {}".format(execution_response))

        # Yeild the execution reponse and the py-exectuor framework will
        # include it in the NB response JSON.
        yield execution_response

    def recover(self, runtime_exception, execution_request):
        """
        Method will get invoked incase the process method fails with
                some exception.
        """
        return None

    def send_notification(self, execution_request):
        """
        Method is to send notifications. Just send simple response message.
        """
        yield executor_utils.send_notification(execution_request,
                                               "I am notification")

    def tf_command_executor(self, input_json):
        """
        Function is used to execute the terraform actions
        Args:
            input_json(dict): input fields
        Returns:
            response(dict): response of the terraform actions.
        """
        tf_exec_utility = TerraformCommandExecutor(input_json)
        tf_exec_util_resp = tf_exec_utility.tf_override_file()
        if tf_exec_util_resp["response_code"] == 200:
            tf_default_action = {"init":tf_exec_utility.init_plan,
                            "workspace": tf_exec_utility.create_workspace,
                             "plan": tf_exec_utility.validate_plan,
                             "apply": tf_exec_utility.apply_plan,
                             "destroy": tf_exec_utility.destroy_plan}
            response = {"response_data": {}}

            for tf_plan in tf_default_action:
                if tf_plan in ["init", "workspace", "plan"] or \
                          input_json["terraform-action"] == tf_plan:
                    self.logger.info("tf_plan: {}".format(tf_plan))
                    action_response = tf_default_action[tf_plan]()
                    tf_action = "terraform" + tf_plan
                    if action_response["response_code"] == 200:
                        response["response_data"][tf_action+"-status"] = \
                           {"response_code" : str(action_response["response_code"]),
                                            "stdout": action_response["stdout"],
                                            "stderror": action_response["stderr"]}
                        response["response_code"] = 200
                    else:
                        response["response_data"][tf_action+"-status"] = \
                            {"response_code" : str(action_response["response_code"]),
                                            "stdout": action_response["stdout"],
                                            "stderror": action_response["stderr"]}
                        response["response_code"] = 500
                        return response

        else:
            return tf_exec_util_resp

        return response
