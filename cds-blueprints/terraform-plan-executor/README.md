# Step by step instructions to load this CBA

# Assumption
1. CDS up and running

        $ kubectl get pods -n onap | grep cds
        
        dev-cds-blueprints-processor-b8656f584-wn6lh   1/1     Running    6          7d23h
        dev-cds-command-executor-78b98dff4f-8mtnk      1/1     Running    6          7d23h
        dev-cds-db-0                                   1/1     Running    6          7d22h
        dev-cds-py-executor-8499669b99-ph84w           1/1     Running    6          7d22h
        dev-cds-sdc-listener-749896d795-qgp2t          0/1     Init:0/1   464        7d23h
        dev-cds-ui-77f74ff6db-gbgwp                    1/1     Running    7          7d23h

2. **Assumption is that you had extracted or cloned the git repository under the $HOME folder**

# Look into the data dictionary dd.json

	$ cd ~/equinix-pcei-poc/cds-blueprints/terraform-plan-executor	

	$ cat Scripts/dd.json

# Load the dd.json into CDS

	$ cd ../../util-scripts

	$ bash -x ./dd-microk8s.sh ../cds-blueprints/terraform-plan-executor/Scripts/dd.json

# Create CBA Archive

	$ cd ../cds-blueprints/terraform-plan-executor

	$ zip -r terraform-plan-executor.zip *

# Enrich CBA Archive. It makes the CBA portable to future CDS runtime
	
	$ cd ../../util-scripts

	$ bash -x ./enrich-and-download-cds-blueprint.sh ../cds-blueprints/terraform-plan-executor/terraform-plan-executor.zip

# Save/Deploy the CBA Archive into the CDS runtime (CDS desing time ends here)

	$ bash -x ./save-enriched-blueprint.sh /tmp/CBA/ENRICHED-CBA.zip

# Open a new SSH terminal to tail CDS blueprint processor logs

	$ cd ~/equinix-pcei-poc/util-scripts

	$ bash -x ./tail-cds-bp-log.sh

# Open a new SSH terminal to tail CDS py-executor log
	
	$ cd ~/equinix-pcei-poc/util-scripts

	$ bash -x ./tail-cds-py-executor-log.sh


# Follow the below steps to Invoke CDS REST API to execute this CBA

# Update the Request JSON payload

	$ cd ~/equinix-pcei-poc/cds-blueprints/terraform-plan-executor/Scripts

# Copy the template file
	$ cp request-payload.json.template request-payload.json

# Sample "apply" request payload and update the  USER_INPUT_XX values in the JSON file
    # Note that "terraform-action" attribute value should be "apply"
	# USER_INPUT_CDS_PY_EXEC_POD_IP : Use below command to get the CDS pyexecutor IP
    # kubectl get pods -n onap -o wide | grep dev-cds-py-executor | awk '{print $6}' 
    # USER_INPUT_GIT_URL  ex . "https://gitlab.com/api/v4"
	# USER_INPUT_GIT_ACCESS_TOKEN - refer main readme to generate the git access token.
	# USER_INPUT_GIT_PROJECT_ID  : Git terraform project id. ex. 26901776
	# USER_INPUT_GIT_BRANCH - git branch name ex. "development"
	# USER_INPUT_GIT_DOWNLOAD_FOLDER - Always give "/opt/app/onap/blueprints/deploy"
	# USER_INPUT_GIT_ARCHIVE_FILE_NAME - any tar file name ex. "terraform-plans-poc.tar.gz"
	# USER_INPUT_TERRAFORM_VARS_FILE_NAME - override file from git project ex. "terraform.tfvars"
	# USER_INPUT_TERRAFORM_PLAN_FOLDER - give correct git terraform plan folder name ex ."demo-azure-connectivity"

  [request-payload-azure.json.template](Scripts/request-payload-azure.json.template)

	$ vi request-payload.json
	{
		"commonHeader": {
			"originatorId": "System",
			"requestId": "123456",
			"subRequestId": "1234-12234"
		},
		"actionIdentifiers": {
			"blueprintName": "terraform-plan-executor",
			"blueprintVersion": "1.0.0",
			"actionName": "remote-python",
			"mode": "sync"
		},
		"payload": {
			"remote-python-request": {
				"cds-py-exec-pod-ip": "USER_INPUT_CDS_PY_EXEC_POD_IP",
				"git-url": "USER_INPUT_GIT_URL",
				"git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
				"git-project-id": "USER_INPUT_GIT_PROJECT_ID",
				"git-branch": "USER_INPUT_GIT_BRANCH",
				"git-download-folder": "/opt/app/onap/blueprints/deploy",
				"git-archive-file-name": "USER_INPUT_GIT_ARCHIVE_FILE_NAME",
				"terraform-var-file-name": "USER_INPUT_TERRAFORM_VARS_FILE_NAME",
				"terraform-plan-folder": "USER_INPUT_TERRAFORM_PLAN_FOLDER",
				"terraform-variable-override": {
					"USER_INPUT_VAR_NAME_1": "USER_INPUT_VAR_VALUE_1",
					"USER_INPUT_VAR_NAME_2": "USER_INPUT_VAR_VALUE_2"
				},
				"terraform-environment-variables": {
					"ARM_SUBSCRIPTION_ID": "USER_INPUT_ARM_SUBSCRIPTION_ID",
					"ARM_CLIENT_ID": "USER_INPUT_ARM_CLIENT_ID",
					"ARM_CLIENT_SECRET": "USER_INPUT_ARM_CLIENT_SECRET",
					"ARM_TENANT_ID": "USER_INPUT_ARM_TENANT_ID",
					"ARM_ACCESS_KEY": "USER_INPUT_ARM_ACCESS_KEY"
				},
				"terraform-action": "apply",
				"terraform-workspace-name": "USER_INPUT_TERRAFORM_WORKSPACE_NAME"

			}
		}
	}

[request-payload-aws.json.template](Scripts/request-payload-aws.json.template)

	$ vi request-payload.json
	{
		"commonHeader": {
			"originatorId": "System",
			"requestId": "123456",
			"subRequestId": "1234-12234"
		},
		"actionIdentifiers": {
			"blueprintName": "terraform-plan-executor",
			"blueprintVersion": "1.0.0",
			"actionName": "remote-python",
			"mode": "sync"
		},
		"payload": {
			"remote-python-request": {
				"cds-py-exec-pod-ip": "USER_INPUT_CDS_PY_EXEC_POD_IP",
				"git-url": "USER_INPUT_GIT_URL",
				"git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
				"git-project-id": "USER_INPUT_GIT_PROJECT_ID",
				"git-branch": "USER_INPUT_GIT_BRANCH",
				"git-archive-file-name": "USER_INPUT_GIT_ARCHIVE_FILE_NAME",
				"git-download-folder": "USER_INPUT_GIT_DOWNLOAD_FOLDER",
				"terraform-plan-folder": "USER_INPUT_TERRAFORM_PLAN_FOLDER",
				"terraform-var-file-name": "USER_INPUT_TERRAFORM_VARS_FILE_NAME",
				"terraform-environment-variables": {				
				},
				"terraform-variable-override": {
					equinix_client_id": "USER_INPUT_EQUINIX_CLIENT_ID",
					"equinix_client_secret": "USER_INPUT_EQUINIX_CLIENT_SECRET",
					"equinix_port_name": "USER_INPUT_EQUINIX_PORT_NAME",
					"aws_access_key": "USER_INPUT_AWS_ACCESS_KEY",
					"aws_secret_key": "USER_INPUT_AWS_SECRET_KEY",
					"aws_region": "USER_INPUT_AWS_REGION",
					"aws_metro_code":"USER_INPUT_AWS_METRO_CODE",
					"aws_account_id":"USER_INPUT_AWS_ACCOUNT_ID"
				},
				"terraform-action": "apply",
				"terraform-workspace-name": "USER_INPUT_TERRAFORM_WORKSPACE_NAME"
			}
		}
	}
	
[request-payload-openstack.json.template](Scripts/request-payload-openstack.json.template)

	$ vi request-payload.json
	{
		"commonHeader": {
			"originatorId": "System",
			"requestId": "123456",
			"subRequestId": "1234-12234"
		},
		"actionIdentifiers": {
			"blueprintName": "terraform-plan-executor",
			"blueprintVersion": "1.0.0",
			"actionName": "remote-python",
			"mode": "sync"
		},
		"payload": {
			"remote-python-request": {
				"cds-py-exec-pod-ip": "USER_INPUT_CDS_PY_EXEC_POD_IP",
				"git-url": "USER_INPUT_GIT_URL",
				"git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
				"git-project-id": "USER_INPUT_GIT_PROJECT_ID",
				"git-branch": "USER_INPUT_GIT_BRANCH",
				"git-archive-file-name": "USER_INPUT_GIT_ARCHIVE_FILE_NAME",
				"git-download-folder": "USER_INPUT_GIT_DOWNLOAD_FOLDER",
				"terraform-plan-folder": "USER_INPUT_TERRAFORM_PLAN_FOLDER",
				"terraform-var-file-name": "USER_INPUT_TERRAFORM_VARS_FILE_NAME",
				"terraform-environment-variables": {
					"OS_AUTH_URL": "USER_INPUT_OS_AUTH_URL",
					"OS_USERNAME": "USER_INPUT_OS_USERNAME",
					"OS_PASSWORD": "USER_INPUT_OS_PASSWORD",
					"OS_REGION_NAME": "USER_INPUT_OS_REGION_NAME",
					"OS_PROJECT_NAME": "USER_INPUT_OS_PROJECT_NAME",
					"OS_PROJECT_DOMAIN_NAME": "USER_INPUT_OS_PROJECT_DOMAIN_NAME",
					"OS_USER_DOMAIN_NAME": "USER_INPUT_OS_USER_DOMAIN_NAME",
					"OS_IDENTITY_API_VERSION": "USER_INPUT_OS_IDENTITY_API_VERSION"				
				},
				"terraform-variable-override": {
					"USER_INPUT_VARIABLE_NAME": "USER_INPUT_VARIABLE_VALUE"
				},
				"terraform-action": "apply",
				"terraform-workspace-name": "USER_INPUT_TERRAFORM_WORKSPACE_NAME"
			}
		}
	}


# Sample "destroy" request payload and update the  USER_INPUT_XX values in the JSON file
    # Note that "terraform-action" attribute value should be "destory"
    # USER_INPUT_CDS_PY_EXEC_POD_IP : Use below command to get the CDS pyexecutor IP
    # kubectl get pods -n onap -o wide | grep dev-cds-py-executor | awk '{print $6}' 
    # USER_INPUT_GIT_URL  ex . "https://gitlab.com/api/v4"
	# USER_INPUT_GIT_ACCESS_TOKEN - refer main readme to generate the git access token.
	# USER_INPUT_GIT_PROJECT_ID  : Git terraform project id. ex. 26901776
	# USER_INPUT_GIT_BRANCH - git branch name ex. "development"
	# USER_INPUT_GIT_DOWNLOAD_FOLDER - Always give "/opt/app/onap/blueprints/deploy"
	# USER_INPUT_GIT_ARCHIVE_FILE_NAME - any tar file name ex. "terraform-plans-poc.tar.gz"
	# USER_INPUT_TERRAFORM_VARS_FILE_NAME - override file from git project ex. "terraform.tfvars"
	# USER_INPUT_TERRAFORM_PLAN_FOLDER - give correct git terraform plan folder name ex ."demo-azure-connectivity"



[request-payload-azure.json.template](Scripts/request-payload-azure.json.template)

	$ vi request-payload.json
	{
		"commonHeader": {
			"originatorId": "System",
			"requestId": "123456",
			"subRequestId": "1234-12234"
		},
		"actionIdentifiers": {
			"blueprintName": "terraform-plan-executor",
			"blueprintVersion": "1.0.0",
			"actionName": "remote-python",
			"mode": "sync"
		},
		"payload": {
			"remote-python-request": {
				"cds-py-exec-pod-ip": "USER_INPUT_CDS_PY_EXEC_POD_IP",
				"git-url": "USER_INPUT_GIT_URL",
				"git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
				"git-project-id": "USER_INPUT_GIT_PROJECT_ID",
				"git-branch": "USER_INPUT_GIT_BRANCH",
				"git-download-folder": "USER_INPUT_GIT_DOWNLOAD_FOLDER",
				"git-archive-file-name": "USER_INPUT_GIT_ARCHIVE_FILE_NAME",
				"terraform-var-file-name": "USER_INPUT_TERRAFORM_VARS_FILE_NAME",
				"terraform-plan-folder": "USER_INPUT_TERRAFORM_PLAN_FOLDER",
				"terraform-variable-override": {
					"USER_INPUT_VAR_NAME_1": "USER_INPUT_VAR_VALUE_1",
					"USER_INPUT_VAR_NAME_2": "USER_INPUT_VAR_VALUE_2"
				},
				"terraform-environment-variables": {
					"ARM_SUBSCRIPTION_ID": "USER_INPUT_ARM_SUBSCRIPTION_ID",
					"ARM_CLIENT_ID": "USER_INPUT_ARM_CLIENT_ID",
					"ARM_CLIENT_SECRET": "USER_INPUT_ARM_CLIENT_SECRET",
					"ARM_TENANT_ID": "USER_INPUT_ARM_TENANT_ID",
					"ARM_ACCESS_KEY": "USER_INPUT_ARM_ACCESS_KEY"
				},
				"terraform-action": "destroy",
				"terraform-workspace-name": "USER_INPUT_TERRAFORM_WORKSPACE_NAME"

			}
		}
	}


[request-payload-aws.json.template](Scripts/request-payload-aws.json.template)

	$ vi request-payload.json
		{
		"commonHeader": {
			"originatorId": "System",
			"requestId": "123456",
			"subRequestId": "1234-12234"
		},
		"actionIdentifiers": {
			"blueprintName": "terraform-plan-executor",
			"blueprintVersion": "1.0.0",
			"actionName": "remote-python",
			"mode": "sync"
		},
		"payload": {
			"remote-python-request": {
				"cds-py-exec-pod-ip": "USER_INPUT_CDS_PY_EXEC_POD_IP",
				"git-url": "USER_INPUT_GIT_URL",
				"git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
				"git-project-id": "USER_INPUT_GIT_PROJECT_ID",
				"git-branch": "USER_INPUT_GIT_BRANCH",
				"git-archive-file-name": "USER_INPUT_GIT_ARCHIVE_FILE_NAME",
				"git-download-folder": "USER_INPUT_GIT_DOWNLOAD_FOLDER",
				"terraform-plan-folder": "USER_INPUT_TERRAFORM_PLAN_FOLDER",
				"terraform-var-file-name": "USER_INPUT_TERRAFORM_VARS_FILE_NAME",
				"terraform-environment-variables": {				
				},
				"terraform-variable-override": {
					equinix_client_id": "USER_INPUT_EQUINIX_CLIENT_ID",
					"equinix_client_secret": "USER_INPUT_EQUINIX_CLIENT_SECRET",
					"equinix_port_name": "USER_INPUT_EQUINIX_PORT_NAME",
					"aws_access_key": "USER_INPUT_AWS_ACCESS_KEY",
					"aws_secret_key": "USER_INPUT_AWS_SECRET_KEY",
					"aws_region": "USER_INPUT_AWS_REGION",
					"aws_metro_code":"USER_INPUT_AWS_METRO_CODE",
					"aws_account_id":"USER_INPUT_AWS_ACCOUNT_ID"
				},
				"terraform-action": "destroy",
				"terraform-workspace-name": "USER_INPUT_TERRAFORM_WORKSPACE_NAME"
			}
		}
	}
	
	
[request-payload-openstack.json.template](Scripts/request-payload-openstack.json.template)

	$ vi request-payload.json
		{
		"commonHeader": {
			"originatorId": "System",
			"requestId": "123456",
			"subRequestId": "1234-12234"
		},
		"actionIdentifiers": {
			"blueprintName": "terraform-plan-executor",
			"blueprintVersion": "1.0.0",
			"actionName": "remote-python",
			"mode": "sync"
		},
		"payload": {
			"remote-python-request": {
				"cds-py-exec-pod-ip": "USER_INPUT_CDS_PY_EXEC_POD_IP",
				"git-url": "USER_INPUT_GIT_URL",
				"git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
				"git-project-id": "USER_INPUT_GIT_PROJECT_ID",
				"git-branch": "USER_INPUT_GIT_BRANCH",
				"git-archive-file-name": "USER_INPUT_GIT_ARCHIVE_FILE_NAME",
				"git-download-folder": "USER_INPUT_GIT_DOWNLOAD_FOLDER",
				"terraform-plan-folder": "USER_INPUT_TERRAFORM_PLAN_FOLDER",
				"terraform-var-file-name": "USER_INPUT_TERRAFORM_VARS_FILE_NAME",
				"terraform-environment-variables": {
					"OS_AUTH_URL": "USER_INPUT_OS_AUTH_URL",
					"OS_USERNAME": "USER_INPUT_OS_USERNAME",
					"OS_PASSWORD": "USER_INPUT_OS_PASSWORD",
					"OS_REGION_NAME": "USER_INPUT_OS_REGION_NAME",
					"OS_PROJECT_NAME": "USER_INPUT_OS_PROJECT_NAME",
					"OS_PROJECT_DOMAIN_NAME": "USER_INPUT_OS_PROJECT_DOMAIN_NAME",
					"OS_USER_DOMAIN_NAME": "USER_INPUT_OS_USER_DOMAIN_NAME",
					"OS_IDENTITY_API_VERSION": "USER_INPUT_OS_IDENTITY_API_VERSION"				
				},
				"terraform-variable-override": {
					"USER_INPUT_VARIABLE_NAME": "USER_INPUT_VARIABLE_VALUE"
				},
				"terraform-action": "destroy",
				"terraform-workspace-name": "USER_INPUT_TERRAFORM_WORKSPACE_NAME"
			}
		}
	}


# Verify the the request-payload.json

	$ cat request-payload.json
	$ export REQUEST_PAYLOAD="$PWD/request-payload.json"

# Get the CDS blueprint processor service IP
	$ CDS_BP_SVC_IP=$(kubectl get svc -n onap | grep 'cds-blueprints-processor-http' | awk '{print $3}')

	$ echo "CDS Blueprint processor IP = $CDS_BP_SVC_IP"

# Execute the below CURL Command to execute this CBA
    # On success you should get HTTP response code 200

	$ curl -v --location --request POST http://${CDS_BP_SVC_IP}:8080/api/v1/execution-service/process --header 'Content-Type: application/json;charset=UTF-8' --header 'Accept: application/json;charset=UTF-8,application/json' --header 'Authorization: Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==' --header 'Host: cds-blueprints-processor-http:8080' --header 'Content-Type: text/json' --data  "@$REQUEST_PAYLOAD" | python3 -m json.tool

# Now you can switch to the CDS py-executor log console for more details
	
# Now you can switch to the CDS blueprint processor log console for more details
