# Step by step instructions to load this CBA

# Assumption
1. CDS up and running

        $ kubectl get pods -n onap | grep cds
        
        dev-cds-blueprints-processor-b8656f584-wn6lh   1/1     Running    6          7d23h
        dev-cds-command-executor-78b98dff4f-8mtnk      1/1     Running    6          7d23h
        dev-cds-db-0                                   1/1     Running    6          7d22h
        dev-cds-py-executor-8499669b99-ph84w           1/1     Running    6          7d22h
        dev-cds-sdc-listener-749896d795-qgp2t          0/1     Init:0/1   464        7d23h
        dev-cds-ui-77f74ff6db-gbgwp                    1/1     Running    7          7d23h

2. **Assumption is that you had extracted or cloned the git repository under the $HOME folder**

# Look into the data dictionary dd.json

	$ cd ~/cds-blueprints/ansible-playbook-executor	

	$ cat Scripts/dd.json

# Load the dd.json into CDS

	$ cd ../util-scripts

	$ bash -x ./dd-microk8s.sh ../ansible-playbook-executor/Scripts/dd.json

# Create CBA Archive

	$ cd ../ansible-playbook-executor

	$ zip -r ansible-playbook-executor.zip *

# Enrich CBA Archive. It makes the CBA portable to future CDS runtime
	
	$ cd ../util-scripts

	$ bash -x ./enrich-and-download-cds-blueprint.sh ../ansible-playbook-executor/ansible-playbook-executor.zip

# Save/Deploy the CBA Archive into the CDS runtime (CDS desing time ends here)

	$ bash -x ./save-enriched-blueprint.sh /tmp/CBA/ENRICHED-CBA.zip

# Open a new SSH terminal to tail CDS blueprint processor logs

	$ cd ~/cds-blueprints/util-scripts

	$ bash -x ./tail-cds-bp-log.sh

# Open a new SSH terminal to tail CDS py-executor log
	
	$ cd ~/cds-blueprints/util-scripts

	$ bash -x ./tail-cds-py-executor-log.sh


# Follow the below steps to Invoke CDS REST API to execute this CBA

# Update the Request JSON payload

	$ cd ~/cds-blueprints/ansible-playbook-executor/Scripts

# Copy the template file
	$ cp request-payload.json.template request-payload.json

# Sample request payload and update the  USER_INPUT_XX values in the JSON file
    
	# USER_INPUT_CDS_PY_EXEC_POD_IP : Use below command to get the CDS pyexecutor IP
        # kubectl get pods -n onap -o wide | grep dev-cds-py-executor | awk '{print $6}' 
        # USER_INPUT_GIT_URL  ex . "https://gitlab.com/api/v4"
	# USER_INPUT_GIT_USERNAME - git login user name
	# USER_INPUT_GIT_ACCESS_TOKEN - generate the git access token and paste it here.
	# USER_INPUT_GIT_PROJECT_ID  : Git  project id. ex. 26901776
	# USER_INPUT_GIT_BRANCH - git branch name ex. "development"
	# USER_INPUT_GIT_DOWNLOAD_FOLDER - Always give "/opt/app/onap/blueprints/deploy"
	# USER_INPUT_GIT_ARCHIVE_FILE_NAME - any tar file name ex. "kud_cluster_target_git_repo.tar.gz"
	# USER_INPUT_IP_ADDRESS - metal ip address
	# USER_INPUT_LOGIN_USERNAME - matal user name
	# USER_INPUT_BGP_EDGE_ADDRESS - BGP edge ip address
	# USER_INPUT_KEY_FILE - key file name in the repo
	# USER_INPUT_ANSIBLE_SCRIPT_FOLDER - Script folder name

[execute-ansible-playbook-request-payload.json.template](Templates/execute-ansible-playbook-request-payload.json.template)

	$ vi request-payload.json

        {
        "commonHeader": {
                "originatorId": "System",
                "requestId": "123456",
                "subRequestId": "1234-12234"
        },
        "actionIdentifiers": {
                "blueprintName": "ansible-playbook-executor",
                "blueprintVersion": "1.0.0",
                "actionName": "execute-ansible-playbook",
                "mode": "sync"
        },
        "payload": {
                "execute-ansible-playbook-request": {
                        "implementation": {
                                "timeout": 3000
                        },
                        "cds-py-exec-pod-ip": "USER_INPUT_PY_EXECUTOR_IP",
                        "py-exec-grcp-timeout": "USER_INPUT_CDS_PY_EXEC_GRPC_TIMEOUT_INTGER_MILLI_SECOND",
                        "workflow-name": "execute-ansible-playbook",
                        "skip-input-params-keys": ["ansible-cli-env-variables"],
                        "input-params": {
                                "ssh-key-git-repository": {
                                        "git-user": "USER_INPUT_GIT_USERNAME",
                                        "git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
                                        "git-url": "USER_INPUT_GIT_URL",
                                        "git-branch": "USER_INPUT_GIT_BRANCH",
                                        "git-download-folder": "/opt/app/onap/blueprints",
                                        "git-project-folder": "USER_INPUT_GIT_PROJECT_FOLDER",
                                        "ssh-key-file-name": "USER_INPUT_KEY_FILE"
                                },
                                "ansible-scripts-git-repository": {
                                        "git-user": "USER_INPUT_GIT_USERNAME",
                                        "git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
                                        "git-url": "USER_INPUT_GIT_URL",
                                        "git-branch": "USER_INPUT_GIT_BRANCH",
                                        "git-download-folder": "/opt/app/onap/blueprints",
                                        "git-project-folder": "USER_INPUT_GIT_PROJECT_FOLDER",
                                        "ansible-scripts-folder": "USER_INPUT_ANSIBLE_SCRIPT_FOLDER",
                                        "ansible-main-yaml-file-name": "USER_INPUT_ANSIBLE_YAML_FILE_NAME"
                                },
                                "ansible-cli-env-variables": {
                                        "target_ip": "USER_INPUT_IP_ADDRESS",
                                        "target_host_user": "USER_INPUT_LOGIN_USERNAME",
                                        "bgp_edge_address": "USER_INPUT_BGP_EDGE_ADDRESS",
                                        "localhost_kud_config_realtive_download_folder": ".kube",
                                        "git_url": "USER_INPUT_GIT_URL_WITHOUT_HTTPS",
                                        "git_branch": "USER_INPUT_GIT_BRANCH",
                                        "remote_host_relative_clone_folder": "USER_INPUT_CLONE_TARGET_DIR",
                                        "git_user": "USER_INPUT_GIT_USERNAME",
                                        "git_accesstoken": "USER_INPUT_GIT_ACCESS_TOKEN",
                                        "git_user_email": "USER_INPUT_GIT_EMAIL_ID",
                                        "customer_id":  "USER_INPUT_CUSTOMER_ID"
                                }
                        }
                }
           }
        }


# Verify the the request-payload.json

	$ cat request-payload.json
	$ export REQUEST_PAYLOAD="$PWD/request-payload.json"

# Get the CDS blueprint processor service IP
	$ CDS_BP_SVC_IP=$(kubectl get svc -n onap | grep 'cds-blueprints-processor-http' | awk '{print $3}')

	$ echo "CDS Blueprint processor IP = $CDS_BP_SVC_IP"

# Execute the below CURL Command to execute this CBA
    # On success you should get HTTP response code 200

	$ curl -v --location --request POST http://${CDS_BP_SVC_IP}:8080/api/v1/execution-service/process --header 'Content-Type: application/json;charset=UTF-8' --header 'Accept: application/json;charset=UTF-8,application/json' --header 'Authorization: Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==' --header 'Host: cds-blueprints-processor-http:8080' --header 'Content-Type: text/json' --data  "@$REQUEST_PAYLOAD" | python3 -m json.tool

# Now you can switch to the CDS py-executor log console for more details
	
# Now you can switch to the CDS blueprint processor log console for more details
