"""
Generic Input Parameter Validator
"""
import json
import logging
import os


class GenericInputParameterValidator:
    """
    Generic Input Parameter Validator
    """

    def __init__(self, work_flow_name, request_input_params, skip_variables):
        """

        :param work_flow_name:
        :param request_input_params:
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("GenericInputParameterValidator init method called")
        self.request_input_params = request_input_params
        self.work_flow_name = work_flow_name
        self.json_file_name = f'{work_flow_name}-request-payload.json.template'
        self.missing_keys = []
        self.template_file = ""
        self.skip_variables = skip_variables

    def process_validation(self):
        """

        :return:
        """
        # read data from the json file
        workflow = f"{self.work_flow_name}-request"
        self.logger.info("Workflow request : %s", workflow)
        json_data = self.read_json_file()
        self.logger.info("Json data from the template file: **%s", json_data)
        template_input_params = json_data.get("payload").get(workflow).get(
            "input-params")
        missing_key_lst = self.validate_input_params(template_input_params,
                                                     self.request_input_params)
        self.logger.info("Missing in key list %s", missing_key_lst)
        if not missing_key_lst:
            return {
                'status': True,
                'missing_key_lst': missing_key_lst,
                "templat_file_name": self.template_file
            }
        return {
            'status': False,
            'missing_key_lst': missing_key_lst,
            "templat_file_name": self.template_file
        }

    def read_json_file(self):
        """

        :return:
        """
        script_dir = os.path.abspath(os.path.join(__file__, "../../.."))
        json_file_path = f"{script_dir}/Templates/{self.json_file_name}"
        self.template_file = f"Templates/{self.json_file_name}"
        self.logger.info("Json file path :%s", json_file_path)
        # self.json_file_name = 'test_template.json.template'
        with open(json_file_path, 'r') as outfile:
            template_data = json.load(outfile)
        return template_data

    def validate_input_params(self, template_payload, request_payload):
        """

        :param template_payload:
        :param request_payload:
        :return:
        """
        str1, str2 = set(template_payload), set(request_payload)
        in1, in12, in2 = str1 - str2, str1 & str2, str2 - str1
        if in1:
            # print('Keys only in 1:', sorted(in1))
            self.logger.info(
                "Please check the request payload sample templates"
                " available in the template folder")
            self.logger.info("key is missing in "
                             "the request payload: %s", sorted(in1))
            # print("key is missing in the request payload", sorted(in1))
            # Adding the missing key in missing key list
            for key in in1:
                self.missing_keys.append(key)
                # print(key)

        if in2:
            # print('Keys only in 2:', sorted(in2))
            # It will print the new key
            self.logger.info("New key found", sorted(in2))
            # print("New key found", sorted(in2))

        same_value = {
            key
            for key in in12 if template_payload[key] == request_payload[key]
        }
        diff_value = in12 - same_value
        diff_dict = {
            key
            for key in diff_value if
            type(template_payload[key]) == type(request_payload[key]) == dict
        }

        for key in sorted(diff_dict):

            if key in self.skip_variables:
                continue
            else:
                # print('## START dictdiff for common key', key)
                self.validate_input_params(template_payload[key],
                                           request_payload[key])
                # print('## STOP  dictdiff for common key', key)
        return self.missing_keys

    def check_empty_value(self, request_dict):
        """

        :param request_dict:
        :return:
        """
        for k, value in request_dict.items():
            if isinstance(value, dict):
                if not value:
                    # print("***************{0} : {1}".format(k, value))
                    self.logger.info("***************%s : %s", k, value)
                else:
                    self.check_empty_value(value)
            else:
                # print("{0} : {1}".format(k, value))
                if not value:
                    # print("***************{0} : {1}".format(k, value))
                    self.logger.info("***************%s : %s", k, value)
        # TO DO
