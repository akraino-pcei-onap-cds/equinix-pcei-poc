#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
"""
Generic ansible executor
"""

import json
import logging
import os

from blueprints_grpc import executor_utils
from blueprints_grpc.blueprint_processing_server import AbstractScriptFunction
from google.protobuf import json_format

from .ansible_utility import AnsibleUtility
from .command_executor_utility import CommandExecutor
from .generic_input_parameter_validator import GenericInputParameterValidator
from .git_utility import GitUtility
from .py_exec_response_template import ResponseUtility
from .cba_common_keywords import CbaCommonKeywords


class GenericAnsibleExecutor(AbstractScriptFunction):
    """
    This class is reposible to do the ansible execution.
    """

    def __init__(self):
        """
        Init method
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("GenericAnsibleExecutor init method called")
        self.private_key = None
        self.git_obj = GitUtility()
        self.sub_request_id = None
        self.request_id = None
        self.originator_id = None
        self.response_obj = ResponseUtility()

    def process(self, execution_request):
        """
        This method will be executed by the CDS py-executor micro service
        over GRPC call coming from CDS blueprint processor to the py-executor
        micro service.

        Parameters
        ----------
        execution_request : Dictionary
            It has all attributes sent from NB API payload to the
                        CDS blue print processor.
        """

        self.logger.info("Request Received in Script : %s", execution_request)
        # Convert the input dict into JSON
        inputs = json_format.MessageToJson(execution_request.payload)
        common_data = json.loads(
            json_format.MessageToJson(execution_request.commonHeader))
        self.sub_request_id = common_data.get(
            CbaCommonKeywords.CBA_SUB_REQUEST_ID_KEY)
        self.request_id = common_data.get(CbaCommonKeywords.CBA_REQUEST_ID_KEY)
        self.originator_id = common_data.get(
            CbaCommonKeywords.CBA_ORIGINATOR_ID_KEY)
        action_identifiers = json.loads(
            json_format.MessageToJson(execution_request.actionIdentifiers))

        self.logger.info("Action identifiers dict: %s", action_identifiers)

        input_json = json.loads(
            json_format.MessageToJson(
                execution_request.payload))["GenericAnsibleExecutor-request"]
        workflow_name = input_json.get(CbaCommonKeywords.CBA_WORKFLOW_NAME)
        self.logger.info("Workflow name %s", workflow_name)
        self.logger.info("Request payload json :%s", input_json)

        input_params = input_json.get("input-params")
        self.logger.info("********Input params********:%s***********",
                         input_params)
        skip_input_vars = input_json.get(CbaCommonKeywords.SKIP_INPUT_KEY)
        # This method is to the check input parameter comparing with the template
        input_params_obj = GenericInputParameterValidator(
            workflow_name, input_params, skip_input_vars)
        self.logger.info("Invoke the input params validation")
        validated_info = input_params_obj.process_validation()
        self.logger.info("Validated info: %s", validated_info)
        if validated_info.get('status'):
            # calling the private key download method
            self.logger.info(
                "Going to execute the download private key function")
            private_key_status = self.private_key_download(input_params)

            response_dict = self.response_obj.response_dict(
                self.request_id, self.sub_request_id, self.originator_id)

            result = CbaCommonKeywords.RESULT_SUCCESS_STATUS
            if private_key_status.get("status"):
                # Setting the success response for the private key download
                response_dict["download-private-key-rest-api-status"] = \
                    self.response_obj.rest_api_response(
                    input_params.get("ssh-key-git-repository").get("git-url"),
                    CbaCommonKeywords.RESULT_SUCCESS_STATUS,
                    private_key_status.get("download_resp")
                        .get(CbaCommonKeywords.CBA_RESPONSE_PAYLOAD_KEY),
                    ""
                )

                execution_response = self.extract_tar_ansible_executor(
                    input_params, execution_request, response_dict)
                self.logger.info("extract tar log execution_response : %s",
                                 execution_response)
                if not execution_response.get("status"):
                    execution_response = execution_response.get(
                        "execution_response")
                    result = CbaCommonKeywords.RESULT_FAILURE_STATUS

            else:
                execution_response = executor_utils.failure_response(
                    execution_request, private_key_status,
                    CbaCommonKeywords.BP_BAD_REQUEST_STATUS_CODE,
                    "Failed to download from GIT")
                result = CbaCommonKeywords.RESULT_FAILURE_STATUS
        else:
            # Failure Case
            result = CbaCommonKeywords.RESULT_FAILURE_STATUS
            template_file_name = validated_info.get("templat_file_name")
            error_message = "Please check the request payload missing required key " \
                            f"sample templates available in the folder {template_file_name}"
            self.logger.error("***Input validation failed***")
            self.logger.error(error_message)
            execution_response = executor_utils.failure_response(
                execution_request, validated_info,
                CbaCommonKeywords.BP_BAD_REQUEST_STATUS_CODE, error_message)

        if result == CbaCommonKeywords.RESULT_SUCCESS_STATUS:
            execution_response = executor_utils.success_response(
                execution_request, response_dict,
                CbaCommonKeywords.BP_SUCCESS_STATUS_CODE)
        self.logger.info("Response returned : %s", execution_response)
        yield execution_response

    def recover(self, runtime_exception, execution_request):
        """
        Method will get invoked in case the process method fails with
                some exception.
        """
        return None

    def send_notification(self, execution_request):
        """
        Method is to send notifications. Just send simple response message.
        """
        yield executor_utils.send_notification(execution_request,
                                               "I am notification")

    def private_key_download(self, input_params):
        """
        :params input_params: input params form the request payload
        :return:
        """
        download_ssh_key_file_info = input_params.get('ssh-key-git-repository')
        self.logger.info("Input params for the download ssh key : %s",
                         download_ssh_key_file_info)
        ssh_download_resp = self.git_obj.git_clone(download_ssh_key_file_info,
                                                   self.sub_request_id)

        if ssh_download_resp[
                'response-code'] == CbaCommonKeywords.SUCCESS_STATUS_CODE:
            ssh_key_repo_path = ssh_download_resp.get('repo_path')
            ssk_key_file = input_params.get('ssh-key-git-repository').get(
                'ssh-key-file-name')
            self.private_key = f'{ssh_key_repo_path}/{ssk_key_file}'
            self.logger.info("Download key file full path: %s",
                             self.private_key)

            # Change the file permission for the private key
            cmd_string = f"chmod 400 {self.private_key}"
            cmd_obj = CommandExecutor()
            command_str, stdout, stderr, status_code = cmd_obj.command_executor(
                cmd_string)
            self.logger.info("***private key path**: %s", self.private_key)
            if status_code == 0:
                self.logger.info(
                    "Successfully completed the key file download")
                return {"status": True, "download_resp": ssh_download_resp}
            else:
                cmd_resp = self.response_obj.command_response(
                    command_str, status_code, stdout, stderr,
                    CbaCommonKeywords.FAILURE_STATUS_CODE)
                return {"status": False, "download_resp": cmd_resp}
        else:
            return {"status": False, "download_resp": ssh_download_resp}

    def extract_tar_ansible_executor(self, input_params, execution_request,
                                     response_dict):
        """
        :param input_params:
        :param execution_request:
        :param response_dict:
        :return:
        """
        download_ansible_input_json = input_params.get(
            'ansible-scripts-git-repository')

        # Here we are downloading the ansible git repo
        ansible_download_resp = self.git_obj.git_clone(
            download_ansible_input_json, self.sub_request_id)

        if ansible_download_resp[
                "response-code"] == CbaCommonKeywords.SUCCESS_STATUS_CODE:
            response_dict[
                "ansible-script-download-rest-api-status"] = self.response_obj.rest_api_response(
                    input_params.get("ansible-scripts-git-repository").get(
                        "git-url"), ansible_download_resp.get("status"),
                    ansible_download_resp.get(
                        CbaCommonKeywords.CBA_RESPONSE_PAYLOAD_KEY), "")
            # On success of git download
            ansible_repo_path = ansible_download_resp.get('repo_path')
            ansible_folder = input_params.get(
                'ansible-scripts-git-repository').get('ansible-scripts-folder')
            self.logger.info("Ansible repo path folder path: %s",
                             ansible_repo_path)
            self.logger.info("Ansible folder path: %s", ansible_folder)
            extracted_full_path = f'{ansible_repo_path}/{ansible_folder}'
            # changing the dir to the ansible script dir
            os.chdir(extracted_full_path)
            ans_obj = AnsibleUtility()
            target_ip = input_params.get("ansible-cli-env-variables").get(
                "target_ip")
            ansible_env_variable = input_params.get(
                "ansible-cli-env-variables")
            private_key = self.private_key
            ansible_main_yaml_file = input_params.get("ansible-scripts-git-repository").\
                get("ansible-main-yaml-file-name")
            kud_target_path_str = input_params.get(
                "ansible-cli-env-variables").get("kud_target_folder_path")
            kud_target_folder_path = f'{kud_target_path_str}/{self.sub_request_id}'
            self.logger.info(
                "Kud target folder path :"
                "kud_target_folder_path : %s", kud_target_folder_path)
            # ansible command execute method going to call
            command_str, stdout, stderr, status_code = ans_obj.ansible_execute(
                target_ip, ansible_env_variable, private_key,
                ansible_main_yaml_file)

            if status_code == 0:
                # on success of the ansible execute
                # store the kud config into the git repo
                response_dict[
                    "ansible-execute-cmd-status"] = self.response_obj.command_response(
                        command_str, str(status_code), "", "",
                        CbaCommonKeywords.SUCCESS_STATUS_CODE)
                # TO DOO
                return {
                    "execution_response":
                    response_dict["ansible-execute-cmd-status"],
                    'status':
                    True
                }
            else:
                data = "Execute ansible is failed"
                execution_response = executor_utils.failure_response(
                    execution_request, data,
                    CbaCommonKeywords.BP_BAD_REQUEST_STATUS_CODE,
                    "Execute ansible is failed")
                return {
                    "execution_response": execution_response,
                    'status': False
                }
        else:
            self.logger.info("Failed to download the file from git repository")
            execution_response = executor_utils.failure_response(
                execution_request, ansible_download_resp["response-data"],
                CbaCommonKeywords.BP_BAD_REQUEST_STATUS_CODE,
                "Failed to download from GIT")
            result = CbaCommonKeywords.RESULT_FAILURE_STATUS

            return {"execution_response": execution_response, 'status': False}
