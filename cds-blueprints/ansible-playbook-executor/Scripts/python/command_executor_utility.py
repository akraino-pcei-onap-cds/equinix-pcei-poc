#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
# pylint:disable=R0903
"""
Command executor utility
"""
import logging
import subprocess


class CommandExecutor:
    """
    This class is mainly responsible to execute given commands.
    """

    def __init__(self):
        """
        Init method
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("CommandExecutor init method called")

    def command_executor(self, command_str):
        """
        Function is used to execute the given command
        Args:
            command_str(str) : command string which is going to execute as
                               one subprocess.
        Returns:
            (boolean): true or false
        """
        process = subprocess.Popen(command_str,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   shell=True,
                                   encoding="utf-8")
        stdout, stderr = process.communicate()
        status_code = process.wait()
        self.logger.info("Given command: %s", command_str)
        self.logger.info("Command output: %s", stdout)
        self.logger.info("Command error: %s", stderr)
        self.logger.info("Command status code: %s", status_code)

        return command_str, stdout, stderr, status_code
