#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
"""
Git Utility
"""
import logging
import os
import glob

from git import Repo
from .py_exec_response_template import ResponseUtility
from .cba_common_keywords import CbaCommonKeywords


class GitUtility:
    """
    This class is responsible to execute the git commands.
    """

    def __init__(self):
        """
        Init method
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("GitCommandUtility init method called")
        self.resp_obj = ResponseUtility()

    def git_clone(self, input_data, request_id):
        """
        This method is mainly used to download the file/dirs from
        github or gitlab repository
        :input_data(dict): data to download the git repo
        :request_id(str): request id
        :return(dict): response dict
        """
        self.logger.info("************ git download *****************")
        try:
            url = "https://{}:{}@{}".format(
                input_data.get(CbaCommonKeywords.GIT_USER),
                input_data.get(CbaCommonKeywords.GIT_ACCESS_TOKEN),
                input_data.get(CbaCommonKeywords.GIT_URL).split("//")[1])
            self.logger.info("Git repo url: %s",
                             input_data.get(CbaCommonKeywords.GIT_URL))

            download_location = os.path.join(
                input_data.get(CbaCommonKeywords.GIT_DOWNLOAD_FOLDER),
                request_id,
                input_data.get(CbaCommonKeywords.GIT_PROJECT_FOLDER))
            self.logger.info("git download location: %s", download_location)
            self.logger.info(
                "git project downloaded folder: %s",
                input_data.get(CbaCommonKeywords.GIT_PROJECT_FOLDER))
            if not os.path.exists(os.path.dirname(download_location)):
                os.makedirs(os.path.dirname(download_location))
            response = Repo.clone_from(url,
                                       download_location,
                                       branch=input_data.get(
                                           CbaCommonKeywords.GIT_BRANCH))
            self.logger.info("Successfully downloaded from GIT Repository")
            final_resp = self.resp_obj.rest_api_response(
                input_data.get(CbaCommonKeywords.GIT_URL),
                CbaCommonKeywords.RESULT_SUCCESS_STATUS,
                CbaCommonKeywords.SUCCESS_STATUS_CODE, "")
            final_resp["repo_path"] = download_location
            return final_resp

        except Exception as git_err:
            self.logger.info(
                "Got an exception while downlading the git repo: %s", git_err)
            return self.resp_obj.rest_api_response(
                input_data.get(CbaCommonKeywords.GIT_URL),
                CbaCommonKeywords.RESULT_FAILURE_STATUS,
                CbaCommonKeywords.FAILURE_STATUS_CODE,
                "Failed to Clone the git repo")

    def git_upload(self, input_data, request_id, upload_dir):
        """
        This method is responsible to push the files/directories to git repository.
        """
        self.logger.info("************ git upload **************")
        try:
            git_repo_dir = os.path.join(
                input_data.get(CbaCommonKeywords.GIT_DOWNLOAD_FOLDER),
                request_id,
                input_data.get(CbaCommonKeywords.GIT_PROJECT_FOLDER))
            git_repo = Repo(git_repo_dir)

            files_to_upload = glob.glob(upload_dir + "/**/*", recursive=True)
            os.chdir(git_repo_dir)
            git_repo.index.add(files_to_upload)
            git_repo.index.commit(CbaCommonKeywords.GIT_COMMIT_MSG)
            upload_resp = git_repo.git.push(
                'origin', input_data.get(CbaCommonKeywords.GIT_BRANCH))
            if not upload_resp:
                self.logger.info("successfully uploaded to the git repository")
                return self.resp_obj.rest_api_response(
                    input_data.get(CbaCommonKeywords.GIT_URL),
                    CbaCommonKeywords.RESULT_SUCCESS_STATUS,
                    CbaCommonKeywords.SUCCESS_STATUS_CODE, "")

        except Exception as err:
            self.logger.error("Got an exception while git upload: %s", err)
            return self.resp_obj.rest_api_response(
                input_data.get(CbaCommonKeywords.GIT_URL),
                CbaCommonKeywords.RESULT_FAILURE_STATUS,
                CbaCommonKeywords.FAILURE_STATUS_CODE,
                "Failed to Upload file to the git repo")
