#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

# This python file to update the success response key should be updated by this file

# Sample to update the success_response_keys
# CUSTOM_ATTR_KEY1 = ''
# CUSTOM_ATTR_KEY2 = ''
# success_response_keys = [CUSTOM_ATTR_KEY1, CUSTOM_ATTR_KEY2]
# success_response_keys.append[CUSTOM_ATTR_KEY3]
# Write the logic to update the success response
"""
Success response keys
"""
success_response_keys_cmd = []
success_response_keys_api = []
