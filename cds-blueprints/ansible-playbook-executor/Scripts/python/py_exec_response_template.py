#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
# pylint:disable=R0201
"""
Py Exec Response Template
"""
import logging

from .success_response_keys import *


class ResponseUtility:
    """
       This class is responsible to set response.
    """

    def __init__(self):
        """
        Init method
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("ResponseUtility init method called")

    def response_dict(self, request_id, sub_request_id, originator_id):
        """
        This method is set the request information
        :params request_id:
        :params sub_request_id:
        :params originator_id:
        """
        resp_dict = {
            "py-executor-originator-id": originator_id,
            "py-executor-request-id": request_id,
            "py-executor-subrequest-id": sub_request_id,
            "key-response-commands": []
        }
        return resp_dict

    def command_response(self, cmd_string, exit_code, stdout, std_error,
                         response_code):
        """
        This method to set the command response
        :param cmd_string: string
        :param exit_code: string
        :param stdout: string
        :param std_error: string
        :param response_code: int
        """
        cmd_res_template = {
            "command-string": cmd_string,
            "exitcode": exit_code,
            "response-code": response_code,
            "stdout": stdout,
            "stderror": std_error,
            "custom-response-attributes": success_response_keys_cmd
        }
        return cmd_res_template

    def rest_api_response(self, rest_api_endpoint, status, response_code,
                          response_data):
        """
        This method is to set the rest api response
        :params rest_api_endpoint: string
        :params status: string
        :params response_code: string
        :params response_data: string
        """
        rest_api_res_template = {
            "rest-api-end-point": rest_api_endpoint,
            "status": status,
            "response-code": response_code,
            "response-data": response_data,
            "custom-response-attributes": success_response_keys_api
        }
        return rest_api_res_template
