#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# pylint:disable=R0903
"""
Ansible Utility
"""

import logging
from .command_executor_utility import CommandExecutor


class AnsibleUtility:
    """
    This Class is responsible to execute the ansible script
    """

    def __init__(self):
        """
        Init method
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("TarArchiveUtility init method called")

    def ansible_execute(self, target_ip, ansible_env_variables, private_key,
                        ansible_main_yaml_file):
        """
        This method is responsible to execute the ansible playbook to bring up the cluster
        :param target_ip:
        :param ansible_env_variables:
        :param private_key:
        :param ansible_main_yaml_file:
        :return: true or false
        """
        cmd_obj = CommandExecutor()

        cmd_string = f'ansible-playbook -vv {ansible_main_yaml_file} -i {target_ip}'

        for i, k in ansible_env_variables.items():
            cmd_string += f'-e "{i}={k}" '

        cmd_string += f'--private-key {private_key} --ssh-common-args="-o StrictHostKeyChecking=no"'

        self.logger.info("The given ansible command string: %s", cmd_string)
        command_str, stdout, stderr, status_code = cmd_obj.command_executor(
            cmd_string)
        self.logger.info("Command information status: %s", status_code)

        return command_str, stdout, stderr, status_code
