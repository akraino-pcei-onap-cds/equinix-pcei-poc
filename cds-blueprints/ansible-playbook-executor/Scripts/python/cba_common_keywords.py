#!/usr/bin/python
#
#  Copyright © 2021 Aarna Networks USA.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# pylint:disable=R0903
"""
CBA common keywords
"""


class CbaCommonKeywords:
    """
    Common key words
    """
    CBA_RESPONSE_PAYLOAD_KEY = "response-code"
    REST_ENDPOINT_URL_KEY = "end_point_url"
    CBA_SUB_REQUEST_ID_KEY = "subRequestId"
    CBA_REQUEST_ID_KEY = "requestId"
    CBA_ORIGINATOR_ID_KEY = "originatorId"
    RESULT_SUCCESS_STATUS = "success"
    RESULT_FAILURE_STATUS = "failure"
    CBA_WORKFLOW_NAME = "workflow-name"
    SUCCESS_STATUS_CODE = "200"
    FAILURE_STATUS_CODE = "500"
    BAD_REQUEST_STATUS_CODE = "400"
    UNAUTHORISED_STATUS_CODE = "401"
    BP_SUCCESS_STATUS_CODE = int(200)
    BP_FAILURE_STATUS_CODE = int(500)
    BP_BAD_REQUEST_STATUS_CODE = int(400)
    BP_UNAUTHORISED_STATUS_CODE = int(401)
    GIT_COMMIT_MSG = "File/dir uploaded to git repo"
    GIT_URL = "git-url"
    GIT_USER = "git-user"
    GIT_ACCESS_TOKEN = "git-access-token"
    GIT_BRANCH = "git-branch"
    GIT_DOWNLOAD_FOLDER = "git-download-folder"
    GIT_PROJECT_FOLDER = "git-project-folder"
    KEY_RESPONSE_COMMANDS = "key-response-commands"
    COMMAND_NAME = "command-name"
    COMMAND_TYPE = "command-type"
    INPUT_PARAMS = "input-params"
    GIT_PROJECT_DETAILS = "git-project-details"
    SUCCESS_CMD_EXITCODE = "0"
    FAILURE_CMD_EXITCODE = "1"
    KUD_CONFIG_FILE_NAME = "config"
    SKIP_INPUT_KEY = "skip-input-params-keys-validation"
