/*
 * Copyright © 2020-2021 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aarna.emco.cluster.reg.scripts

import org.slf4j.LoggerFactory

/**
 * Constant file
 */
class EmcoApiServiceConstants {

    companion object {

        private val log = LoggerFactory.getLogger(
            EmcoApiServiceConstants::class.java
        )

        const val VAR_CREATE_CLUSTER_PROVIDER_REQUEST =
            "emco-k8s-cluster-register-workflow-request"
        const val VAR_CREATE_CLUSTER_PREFIX_URL =
            "emco-v2-api-url-prefix"
        const val VAR_GIT_FILE_DOWNLOAD_URL = "git-url"
        const val VAR_GIT_FILE_DOWNLOAD_ACCESS_TOKEN = "git-access-token"
        const val VAR_GIT_FILE_DOWNLOAD_PATH = "git-download-folder"
        const val VAR_CREATE_CLUSTER_PROVIDER_PROPERTIES =
            "emco-k8s-cluster-register-workflow-properties"
        const val VAR_CREATE_CLUSTER_PROVIDER_PARAMETERS =
            "emco-v2-cluster-provider-parameters"
        const val VAR_CREATE_CLUSTER_PARAMETERS =
            "emco-v2-cluster-parameters"
        const val VAR_CREATE_CLUSTER_LABEL_PARAMETERS =
            "emco-v2-cluster-label-parameters"
        const val VAR_CREATE_CLUSTER_KEYVALUE_PARAMETERS =
            "emco-v2-cluster-key-value-parameters"

        const val VAR_CREATE_CLUSTER_SUFFIX_URL =
            "emco-v2-api-suffix"

        const val VAR_CREATE_CLUSTER_PROVIDER_PAYLOAD =
            "cluster-providers-payload"

        const val VAR_CREATE_CLUSTER_PAYLOAD =
            "cluster-providers-cluster-payload"

        const val VAR_CREATE_CLUSTER_LABEL_PAYLOAD =
            "cluster-label-payload"

        const val VAR_CREATE_CLUSTER_KEYVALUE_PAYLOAD =
            "cluster-kv-pair-payload"

        const val VAR_CLUSTER_PROVIDER_URL = "/cluster-providers"

        const val VAR_CLUSTER_URL = "/clusters"

        const val VAR_CLUSTER_LABEL_URL = "/labels"
        const val VAR_CLUSTER_KEYVALUE_URL = "/kv-pairs"

        const val VAR_METADATA = "metadata"
        const val VAR_METADATA_NAME = "name"
        const val VAR_SUCCESS = "success"
        const val VAR_FAIL = "fail"
        const val VAR_ERROR_DATA = "error-data"

        const val VAR_KUBE_CONFIG_FILE_PATH = "/tmp/CBA/kubeconfig"
    }
}
