/*
 * Copyright © 2017-2020 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aarna.emco.cluster.reg.scripts

import com.google.gson.Gson
import org.onap.ccsdk.cds.blueprintsprocessor.core.api.data.ExecutionServiceInput
import org.onap.ccsdk.cds.blueprintsprocessor.services.execution.AbstractScriptComponentFunction
import org.onap.ccsdk.cds.controllerblueprints.core.BluePrintException
import org.onap.ccsdk.cds.controllerblueprints.core.asJsonType
import org.slf4j.LoggerFactory

/**
 * Service API class which creates cluster label for a given cluster
 * provider and cluster
 */
open class EmcoApiClusterLabelService : AbstractScriptComponentFunction() {

    private val log = LoggerFactory.getLogger(EmcoApiClusterLabelService::class.java)

    /**
     * @see ExecutionServiceInput
     */
    override suspend fun processNB(executionRequest: ExecutionServiceInput) {

        val requestPayload = executionRequest.payload
        log.info("Step : =")
        log.info("Execution Request : $requestPayload")

        // extract the request parameters from request payload
        val tempPath = requestPayload
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PROVIDER_REQUEST)
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PROVIDER_PROPERTIES)

        log.info("tempPath :==: $tempPath")
        val baseUrl = tempPath
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PREFIX_URL)
            .textValue()
        val labelParameters = tempPath
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_LABEL_PARAMETERS)

        val labelPayload = labelParameters
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_LABEL_PAYLOAD)

        val providerParameters = tempPath
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PROVIDER_PARAMETERS)
        val providerPayload = providerParameters
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PROVIDER_PAYLOAD)
        val providerName = providerPayload
            .path(EmcoApiServiceConstants.VAR_METADATA)
            .path(EmcoApiServiceConstants.VAR_METADATA_NAME).textValue()

        val clusterParameters = tempPath
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PARAMETERS)

        val clusterPayload = clusterParameters
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PAYLOAD)

        val clusterName = clusterPayload
            .path(EmcoApiServiceConstants.VAR_METADATA)
            .path(EmcoApiServiceConstants.VAR_METADATA_NAME).textValue()

        val payloadRequest = labelPayload.toString()

        val providerUrl: String = EmcoApiServiceConstants
            .VAR_CLUSTER_PROVIDER_URL

        val clusUrl: String = EmcoApiServiceConstants.VAR_CLUSTER_URL

        val tempLabel: String = EmcoApiServiceConstants.VAR_CLUSTER_LABEL_URL

        val labelUrl: String =
            "$baseUrl$providerUrl/$providerName$clusUrl/$clusterName$tempLabel"

        log.info("baseUrl :==: $baseUrl")
        log.info("labelParameters :==: $labelParameters")
        log.info("clusterName :==: $clusterName")
        log.info("providerName :==: $providerName")
        log.info("labelPayload :==: $labelPayload")
        log.info("labelUrl :==: $labelUrl")
        log.info("payloadRequest :==: $payloadRequest")
        log.info("providerName : == $providerName")

        val postRes: HashMap<String, String> = RestUtils.restPost(
            labelUrl, payloadRequest
        )
        log.info("cluster label post done")

        if (postRes.isNotEmpty()) {
            val gson = Gson()
            val jsonResponse = gson.toJson(postRes)
            log.info("Cluster Provider label Created Successfully")
            log.info(
                "Cluster Provider label Created Json response := $jsonResponse"
            )
            if (postRes.get("status") == EmcoApiServiceConstants.VAR_SUCCESS) {
                setAttribute(
                    "response-data", jsonResponse.asJsonType()
                )
                return
            } else {
                setAttribute(
                    "error-data", jsonResponse.asJsonType()
                )
            }
        }
        throw BluePrintException("Cluster label creation failed")
    }

    /**
     * @param runtimeException consists exception details if there any exception
     * @param executionRequest request payload
     * @see ExecutionServiceInput
     */
    override suspend fun recoverNB(
        runtimeException: RuntimeException,
        executionRequest: ExecutionServiceInput
    ) {
        log.info("Executing Recovery")
        addError("${runtimeException.message}")
    }
}
