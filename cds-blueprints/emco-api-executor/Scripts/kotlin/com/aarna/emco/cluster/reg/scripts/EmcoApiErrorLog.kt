/*
 * Copyright © 2021 Orange
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aarna.emco.cluster.reg.scripts

import com.fasterxml.jackson.databind.JsonNode
import org.onap.ccsdk.cds.blueprintsprocessor.core.api.data.ExecutionServiceInput
import org.onap.ccsdk.cds.blueprintsprocessor.services.execution.AbstractScriptComponentFunction
import org.onap.ccsdk.cds.blueprintsprocessor.services.execution.ComponentScriptExecutor
import org.onap.ccsdk.cds.controllerblueprints.core.BluePrintProcessorException
import org.onap.ccsdk.cds.controllerblueprints.core.asJsonNode
import org.slf4j.LoggerFactory

/**
 * class collects the error response data and this output data to response-data
 */
open class EmcoApiErrorLog : AbstractScriptComponentFunction() {

    private val log = LoggerFactory.getLogger(EmcoApiErrorLog::class.java)!!

    /**
     *
     */
    override fun getName(): String {
        return "EmcoApiErrorLog"
    }

    /**
     * Method which sets the error response into the response
     */
    override suspend fun processNB(executionRequest: ExecutionServiceInput) {
        log.info("Inside EmcoApiErrorLog")
        log.info("SIMPLE ERROR CHECK - START")
        bluePrintRuntimeService.bluePrintContext()
            .serviceTemplate.topologyTemplate!!.nodeTemplates!!
            .keys.filter { it.startsWith("emco-k8s-cluster") }
            .associateWith { errorData(it) }
            .let { it.asJsonNode() }
            .also { log.info("Error Collected results: $it") }
            .let { setAttribute(ComponentScriptExecutor.ATTRIBUTE_RESPONSE_DATA, it) }
        log.info("SIMPLE ERROR CHECK - END")
    }

    /**
     * get the error data from each service API error response
     */
    private fun errorData(nodeTemplateName: String): JsonNode? {
        return try {
            bluePrintRuntimeService.getNodeTemplateAttributeValue(
                nodeTemplateName,
                EmcoApiServiceConstants.VAR_ERROR_DATA
            )
        } catch (exception: BluePrintProcessorException) { null }
    }

    /**
     * recover method
     */
    override suspend fun recoverNB(
        runtimeException: RuntimeException,
        executionRequest: ExecutionServiceInput
    ) {
        log.info("Executing Recovery")
        this.addError("${runtimeException.message}")
    }
}
