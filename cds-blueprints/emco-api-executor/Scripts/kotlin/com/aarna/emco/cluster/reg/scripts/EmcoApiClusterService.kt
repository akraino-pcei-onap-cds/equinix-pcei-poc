/*
 * Copyright © 2017-2020 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aarna.emco.cluster.reg.scripts

import com.google.gson.Gson
import org.onap.ccsdk.cds.blueprintsprocessor.core.api.data.ExecutionServiceInput
import org.onap.ccsdk.cds.blueprintsprocessor.services.execution.AbstractScriptComponentFunction
import org.onap.ccsdk.cds.controllerblueprints.core.BluePrintException
import org.onap.ccsdk.cds.controllerblueprints.core.asJsonType
import org.slf4j.LoggerFactory

/**
 * Service API class which creates a cluster for a given cluster provider
 */
open class EmcoApiClusterService : AbstractScriptComponentFunction() {

    private val log = LoggerFactory.getLogger(EmcoApiClusterService::class.java)

    /**
     * @see ExecutionServiceInput
     */
    override suspend fun processNB(executionRequest: ExecutionServiceInput) {

        val requestPayload = executionRequest.payload
        log.info("Step : =")
        log.info("Execution Request : $requestPayload")

        // extract the request parameters from request payload
        val tempPath = requestPayload
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PROVIDER_REQUEST)
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PROVIDER_PROPERTIES)

        log.info("tempPath :==: $tempPath")
        val baseUrl = tempPath
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PREFIX_URL)
            .textValue()
        val gitDownloadPath = tempPath
            .path(EmcoApiServiceConstants.VAR_GIT_FILE_DOWNLOAD_PATH)
            .textValue()

        val clusterParameters = tempPath
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PARAMETERS)

        val clusterPayload = clusterParameters
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PAYLOAD)

        val providerParameters = tempPath
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PROVIDER_PARAMETERS)
        val providerPayload = providerParameters
            .path(EmcoApiServiceConstants.VAR_CREATE_CLUSTER_PROVIDER_PAYLOAD)
        val providerName = providerPayload
            .path(EmcoApiServiceConstants.VAR_METADATA)
            .path(EmcoApiServiceConstants.VAR_METADATA_NAME).textValue()

        val payloadRequest = clusterPayload.toString()

        val providerUrl: String = EmcoApiServiceConstants
            .VAR_CLUSTER_PROVIDER_URL

        val tempClusterUrl: String = EmcoApiServiceConstants.VAR_CLUSTER_URL

        val clusterUrl: String = "$baseUrl$providerUrl/$providerName$tempClusterUrl"

        log.info("baseUrl :==: $baseUrl")
        log.info("clusterParameters :==: $clusterParameters")
        log.info("clusterPayload :==: $clusterPayload")
        log.info("clusterUrl :==: $clusterUrl")
        log.info("payloadRequest :==: $payloadRequest")
        log.info("providerName : == $providerName")
        log.info("gitdownloadpath : == $gitDownloadPath")

        val configFileUrl = gitDownloadPath

        log.info("configFileUrl : $configFileUrl")
        val postRes: HashMap<String, String> = RestUtils
            .uploadPost(clusterUrl, configFileUrl, payloadRequest)

        if (postRes.isNotEmpty()) {
            log.info("Cluster Created Successfully")
            val gson = Gson()
            val jsonResponse = gson.toJson(postRes)
            log.info("Cluster Created Successfully")
            log.info("Cluster Created Json response : = $jsonResponse")
            if (postRes.get("status") == EmcoApiServiceConstants.VAR_SUCCESS) {
                setAttribute(
                    "response-data", jsonResponse.asJsonType()
                )
                return
            } else {
                setAttribute(
                    "error-data", jsonResponse.asJsonType()
                )
            }
        }
        throw BluePrintException("Cluster creation failed")
    }

    /**
     * @param runtimeException consists exception details if there any exception
     * @param executionRequest request payload
     * @see ExecutionServiceInput
     */
    override suspend fun recoverNB(
        runtimeException: RuntimeException,
        executionRequest: ExecutionServiceInput
    ) {
        log.info("Executing Recovery")
        addError("${runtimeException.message}")
    }
}
