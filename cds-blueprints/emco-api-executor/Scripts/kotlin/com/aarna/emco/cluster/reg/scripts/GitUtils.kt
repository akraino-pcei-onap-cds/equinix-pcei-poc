/*
 * Copyright © 2020-2021 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aarna.emco.cluster.reg.scripts

import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import org.onap.ccsdk.cds.controllerblueprints.core.BluePrintException
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Constant file
 */
class GitUtils {

    companion object {

        private val log = LoggerFactory.getLogger(
            GitUtils::class.java
        )

        /**
         * Rest Get API call to download file from GIT repository
         */
        fun gitGetFile(accessCode: String, baseUrl: String, gitDownloadPath:
        String) {
            log.info("GitUtils : get KubeConfig file from Git repo")
            val tempGitUrl: String = baseUrl
            log.info("tempGitUrl :== $tempGitUrl")
            log.info("accessCode :== $accessCode")
            val gitDownloadUrl: String = "$baseUrl&private_token=$accessCode"

            val httpClient: CloseableHttpClient = HttpClients.createDefault()
            val request = HttpGet(gitDownloadUrl)

            val response: CloseableHttpResponse =
                httpClient.execute(request)
            val statusCode = response.statusLine.statusCode
            if (statusCode != 200) {
                throw BluePrintException("KubeConfig Git download failed")
            }
            val entity: org.apache.http.HttpEntity = response.entity
            val byteArray = EntityUtils.toByteArray(entity)
            val filepath: String = gitDownloadPath
            Files.write(Paths.get("$filepath"), byteArray)
            log.info("entity : $entity")
            log.info("Git KubeConfig download complete")
        }
    }
}
