/*
 * Copyright © 2020-2021 Aarna Networks, Inc.
 *           All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aarna.emco.cluster.reg.scripts

import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.onap.ccsdk.cds.controllerblueprints.core.BluePrintException
import org.slf4j.LoggerFactory
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.util.ResourceUtils
import java.io.File
import java.io.FileInputStream
import java.util.HashMap

/**
 * Constant file
 */
class RestUtils {

    companion object {

        private val log = LoggerFactory.getLogger(
            RestUtils::class.java
        )

        const val VAR_SUCCESS = "success"
        const val VAR_FAIL = "fail"

        /**
         * Restful Post utility method
         */
        fun restPost(requestUrl: String, payloadRequest: String):
            HashMap<String, String> {
                log.info(" REST POST started")
                var tempMap: HashMap<String, String> = HashMap()
                // construct cluster providers rest API
                val builder = RestTemplateBuilder()
                val headers = HttpHeaders()

                headers.setContentType(MediaType.APPLICATION_JSON)
                val entity: HttpEntity<String> = HttpEntity(
                    payloadRequest, headers
                )
                try {
                    val response: ResponseEntity<String> = builder.build()
                        .exchange(
                            requestUrl, HttpMethod.POST, entity,
                            String::class.java
                        )
                    val statusCode: Int = response.statusCodeValue
                    log.info("result :==: $statusCode")
                    tempMap.put("httpStatusCode", statusCode.toString())
                    if (statusCode == 201) {
                        tempMap.put("status", VAR_SUCCESS)
                    } else {
                        tempMap.put("status", VAR_FAIL)
                    }
                    tempMap.put("httpResponse", entity.toString())
                    return tempMap
                } catch (blueprintExp: BluePrintException) {
                    log.info("BlueprintException : = $blueprintExp")
                    tempMap.put("httpResponse", "${blueprintExp.message}")
                } catch (exception: Exception) {
                    log.info("Exception : = $exception")
                    tempMap.put("httpResponse", "${exception.message}")
                }
                return tempMap
            }

        /**
         * upload file utility method
         */
        fun uploadPost(
            regUrl: String,
            fileUrl: String,
            payloadRequest: String
        ): HashMap<String, String> {
            var tempMap: HashMap<String, String> = HashMap()

            val file: File = ResourceUtils.getFile("$fileUrl")
            val httpClient: CloseableHttpClient = HttpClients.createDefault()
            val uploadFile = HttpPost(regUrl)
            val builder = MultipartEntityBuilder.create()
            builder.addTextBody(
                "metadata", payloadRequest, ContentType.TEXT_PLAIN
            )

            // This attaches the file to the POST:
            builder.addBinaryBody(
                "file",
                FileInputStream(file),
                ContentType.APPLICATION_OCTET_STREAM,
                file.name
            )
            val multipart = builder.build()
            uploadFile.entity = multipart
            try {
                val response: CloseableHttpResponse =
                    httpClient.execute(uploadFile)
                val responseEntity: org.apache.http.HttpEntity =
                    response.getEntity()

                val statusCode = response.statusLine.statusCode
                log.info("StatusCode := $statusCode")
                log.info("StatusLine := ${response.statusLine}")
                log.info("responseEntity := $responseEntity")
                log.info("responseEntity content := ${responseEntity.content}")
                tempMap.put("httpStatusCode", statusCode.toString())
                if (statusCode == 201) {
                    tempMap.put("status", VAR_SUCCESS)
                } else {
                    tempMap.put("status", VAR_FAIL)
                }
                tempMap.put("httpResponse", responseEntity.toString())
                return tempMap
            } catch (blueprintExp: BluePrintException) {
                log.info("BlueprintException : = $blueprintExp")
                tempMap.put("httpResponse", "${blueprintExp.message}")
            } catch (exception: Exception) {
                log.info("Exception : = $exception")
                tempMap.put("httpResponse", "${exception.message}")
            }
            return tempMap
        }
    }
}
