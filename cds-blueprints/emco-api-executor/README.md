# Step by step instructions to load this CBA

# Assumptions
  1. CDS up and running 
  
    :~$ kubectl get pods -n onap | grep cds
	dev-cds-blueprints-processor-b8656f584-wn6lh   1/1     Running    6          7d3h
	dev-cds-command-executor-78b98dff4f-8mtnk      1/1     Running    6          7d3h
	dev-cds-db-0                                   1/1     Running    6          7d3h
	dev-cds-py-executor-8499669b99-ph84w           1/1     Running    6          7d3h
	dev-cds-sdc-listener-749896d795-qgp2t          0/1     Init:0/1   350        7d3h
	dev-cds-ui-77f74ff6db-gbgwp                    1/1     Running    7          7d3h


  2. AMCOP 2.0 up and running. Note down AMCOP IP and port no. as this need to be updated in 
     request payload 
	
	ubuntu@node1:~$ kubectl get svc -n onap4k8s
	NAME           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                           AGE
	cdap           NodePort    10.233.39.66    <none>        11011:31011/TCP,11015:31015/TCP   62d
	clm            ClusterIP   10.233.11.58    <none>        9061/TCP                          62d
	configsvc      NodePort    10.233.26.91    <none>        9082:30482/TCP                    62d
	dmaap          NodePort    10.233.8.195    <none>        3904:32392/TCP,3905:30768/TCP     62d
	**emcoui         NodePort    10.233.3.164    <none>        9080:30480/TCP                    62d**
	etcd           ClusterIP   10.233.21.72    <none>        2379/TCP,2380/TCP                 62d
	kafka1         ClusterIP   10.233.58.240   <none>        9092/TCP                          62d
	middleend      NodePort    10.233.37.191   <none>        9081:30481/TCP                    62d
	mongo          ClusterIP   10.233.8.236    <none>        27017/TCP                         62d
	ncm            ClusterIP   10.233.56.217   <none>        9031/TCP                          62d
	orchestrator   ClusterIP   10.233.41.158   <none>        9015/TCP                          62d
	ovnaction      ClusterIP   10.233.2.121    <none>        9053/TCP,9051/TCP                 62d
	rsync          ClusterIP   10.233.40.22    <none>        9041/TCP                          62d
	vescollector   NodePort    10.233.43.127   <none>        8080:31080/TCP                    62d
	zookeeper      ClusterIP   10.233.36.23    <none>        2181/TCP                          62d

  3. Assumption is that you had extracted or cloned the git repository under the $HOME folder

# Look into the data dictionary dd.json

	$ cd ~/equinix-pcei-poc/cds-blueprints/emco-api-executor

	$ cat Scripts/dd.json

# Load the dd.json into CDS

	$ cd ../../util-scripts

	$ bash -x ./dd-microk8s.sh ../cds-blueprints/emco-api-executor/Scripts/dd.json

# Create CBA Archive

	$ cd ../cds-blueprints/emco-api-executor

	$ zip -r emco-api-executor.zip *

# Enrich CBA Archive. It makes it portable to future CDS runtime releases
	
	$ cd ../../util-scripts

	$ bash -x ./enrich-and-download-cds-blueprint.sh ../cds-blueprints/emco-api-executor/emco-api-executor.zip

# Save/Deploy the CBA Archive into the CDS runtime (CDS desing time activity ends here)

	$ bash -x ./save-enriched-blueprint.sh /tmp/CBA/ENRICHED-CBA.zip

# Open a new SSH terminal to tail CDS blueprint processor logs

	$ cd ~/equinix-pcei-poc/util-scripts

	$ ./tail-cds-bp-log.sh

# Update the Request JSON payload

	$ cd ~/equinix-pcei-poc/cds-blueprints/emco-api-executor/Scripts

# Copy the template file
	$ cp register-k8s-cluster-template.json request-payload.json
	
# Sample request payload
    # You can refer to the below EMCO REST API link to get more details about the JSON payload
	# https://wiki.onap.org/display/DW/V2+API+Specification
    # Note:- You should update the attribute values for USER_INPUT_XXX parameters	
	# USER_INPUT_EMCO_V2_API_URL_PREFIX  : here update the amcop installed machine ip with portno(30480).
	# ex : http://<amcop server ip>>:30480/v2
	# USER_INPUT_GIT_URL - git project url where kubconfig file exists 
	# ex : https://gitlab.com/api/v4/projects/27142906/repository/files/edge-k8s-pcei-azure-config/raw?ref=development
	# USER_INPUT_GIT_DOWNLOAD_FOLDER : This shouldn't be modified. Always use "/opt/app/onap/blueprints/deploy/kubeconfig".
	# USER_INPUT_GIT_ACCESS_TOKEN - refer main readme file for "how to generate git access token" steps.
	
    $ vi request-payload.json
	{
	  "actionIdentifiers": {
		"mode": "sync",
		"blueprintName": "emco-api-executor",
		"blueprintVersion": "1.0.0",
		"actionName": "emco-k8s-cluster-register-workflow"
	  },
	  "payload": {
		"emco-k8s-cluster-register-workflow-request": {
		  "emco-k8s-cluster-register-workflow-properties": {
			"emco-v2-api-url-prefix": "USER_INPUT_EMCO_V2_API_URL_PREFIX",
			"git-url": "USER_INPUT_GIT_URL",
			"git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
			"git-download-folder": "/opt/app/onap/blueprints/deploy/kubeconfig",
			"emco-v2-cluster-provider-parameters": {
			  "cluster-providers-payload": {
				"metadata": {
				  "name": "USER_INPUT_NAME",
				  "description": "USER_INPUT_DESCRIPTION",
				  "userData1": "USER_INPUT_USERDATA1",
				  "userData2": "USER_INPUT_USERDATA2"
				}
			  }
			},
			"emco-v2-cluster-parameters": {
			  "cluster-providers-cluster-payload": {
				"metadata": {
				  "name": "USER_INPUT_NAME",
				  "description": "USER_INPUT_DESCRIPTION",
				  "userData1": "USER_INPUT_USERDATA1",
				  "userData2": "USER_INPUT_USERDATA2"
				}
			  }
			},
			"emco-v2-cluster-label-parameters": {
			  "cluster-label-payload": {
				"label-name": "USER_INPUT_LABEL_NAME"
			  }
			}
		  }
		}
	  },
	  "commonHeader": {
		"subRequestId": "143748f9-3cd5-4910-81c9-a4601ff2ea58",
		"requestId": "e5eb1f1e-3386-435d-b290-d49d8af8db4c",
		"originatorId": "SDNC_DG"
	  }
	}

# Update the request-payload.json
	$ cat request-payload.json
	$ export REQUEST_PAYLOAD="$PWD/request-payload.json"

# Get the CDS blueprint processor service IP
	$ CDS_BP_SVC_IP=$(kubectl get svc -n onap | grep 'cds-blueprints-processor-http' | awk '{print $3}')

	$ echo "CDS Blueprint processor IP = $CDS_BP_SVC_IP"

# Execute the below CURL Command to execute this blueprint
    # On success the below command will return HTTP status 200

	$ curl -v --location --request POST http://${CDS_BP_SVC_IP}:8080/api/v1/execution-service/process --header 'Content-Type: application/json;charset=UTF-8' --header 'Accept: application/json;charset=UTF-8,application/json' --header 'Authorization: Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==' --header 'Host: cds-blueprints-processor-http:8080' --header 'Content-Type: text/json' --data  "@$REQUEST_PAYLOAD" | python3 -m json.tool

# Now you can switch to the CDS blueprint processor log console for more information
	
# Look at the AMCOP 2.0 console UI to verify cluster provider displayed.

	http://AMCOP_PORTAL_IP:AMCOP_PORTAL_PORT_NUM/app/admin/clusters 





