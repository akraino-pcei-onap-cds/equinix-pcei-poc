# You should onboard Helm3 charts into EMCO to deploy it on the target cluster.

## The folder ./5g-core-cp contains Free 5G control plane related Helm3 charts

## The folder ./5g-ue-ran-sim contains UE RAN simulator Helm3 chart

## The folder ./5g-upf-loc-api contains the UPF and MEC Location API service Helm3 charts

## The folder ./metal-iot-edge contains the Equinix metal IoT Edge Helm3 chart
