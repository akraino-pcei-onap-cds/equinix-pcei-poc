#!/bin/bash
#=======================================================================
# Copyright (c) 2017-2020 Aarna Networks, Inc.
# All rights reserved.
# ======================================================================
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#           http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ========================================================================

# Script to setup microk8s on the local host
# This script should be executed on Ubuntu 18.04 or later

# TODO : Need to try with version : 1.15


# ONAP supported versions
#                Kubernetes  Helm       kubectl     Docker
#frankfurt       1.15.9      2.16.6     1.15.11     18.09.x
#Honolulu        1.19.9      3.5.2      1.19.9      19.03.x

############# microk8s tools deployment ###########
MICRO_K8S_VERSION="1.15/stable"
DOCKER_VERSION=18.09
KUBECTL_VERSION=1.15.11
HELM_VERSION=2.16.0
ONAP_OOM_BRANCH="frankfurt"

sudo apt-get update -y
sudo apt-get update -y && sudo apt-get install git make file wget vim curl tzdata -y

if [ -d /etc/localtime ]
  then
      echo "Applying the workaround for /etc/localtime"
      # Apply this workaround. Otherwise all PODs will fail
      # Generic POD Error as shown below
      #Error: failed to create containerd task: OCI runtime create failed:
      #container_linux.go:345: starting container process caused
      #"process_linux.go:424: container init caused \"rootfs_linux.go:58:
      #mounting \\\"/etc/localtime\\\" to
      #rootfs \\\"/var/snap/microk8s/common/run/containerd/
      #io.containerd.runtime.v1.linux/k8s.io/cds-db/rootfs\\\"
      #at \\\"/var/snap/microk8s/common/run/containerd/
      #io.containerd.runtime.v1.linux/k8s.io/cds-db/rootfs/usr/share/zoneinfo/UTC\\\"
      #caused \\\"not a directory\\\"\"": unknown

      sudo rm -rf /etc/localtime
      sudo ln -sf /usr/share/zoneinfo/UTC /etc/localtime 
fi

exit

# Install microk8s
echo "###################################################"
echo "Going to install Docker ${MICRO_K8S_VERSION}"
echo "###################################################"
sudo snap install microk8s --classic --channel=${MICRO_K8S_VERSION}

# Enable microk8s features
sudo microk8s enable host-access
sudo microk8s enable storage
#sudo microk8s enable helm
sudo microk8s enable dns

# Include the user to microk8s
sudo usermod -a -G microk8s $USER

sudo snap install helm --classic --channel=2.16/stable
mkdir -p ~/.kube 
sudo microk8s config >~/.kube/config
sudo chown -R $USER:$USER ~/.kube
echo "export KUBECONFIG=~/.kube/config" >> ~/.bashrc


echo "###################################################"
echo "Going to install Docker ${DOCKER_VERSION}"
echo "###################################################"
sudo curl https://releases.rancher.com/install-docker/$DOCKER_VERSION.sh | sudo sh

echo "###################################################"
echo "Going to install Kubectl ${KUBECTL_VERSION}"
echo "###################################################"
cd /tmp
curl -LO https://storage.googleapis.com/kubernetes-release/release/v$KUBECTL_VERSION/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
source ~/.bashrc
kubectl get pods --all-namespaces
cd $HOME


echo "###################################################"
echo "Going to Configure Helm"
echo "###################################################"

sudo microk8s kubectl -n kube-system create serviceaccount tiller
sudo microk8s kubectl create clusterrolebinding tiller-cluster-rule \
--clusterrole=cluster-admin --serviceaccount=kube-system:tiller

sudo microk8s helm init --service-account tiller
sudo microk8s kubectl -n kube-system rollout status deploy/tiller-deploy

sudo chown -R $USER:$USER ~/.helm
helm init --stable-repo-url https://charts.helm.sh/stable

echo "###################################################"
echo "Going to stopn any helm server process running on this system"
echo "###################################################"

ps -ef | grep -v grep | grep -iw helm | awk '{print $2}' | xargs kill -9

echo "###################################################"
echo "Going to start helm server"
echo "###################################################"

cd $HOME
nohup helm serve >helm-server.log &
helm repo remove stable


echo "###################################################"
echo "Going to clone ONAP OOM GIT Repository for ${ONAP_OOM_BRANCH}"
echo "###################################################"

cd $HOME
git clone -b ${ONAP_OOM_BRANCH} http://gerrit.onap.org/r/oom --recurse-submodules

echo "###################################################"
echo "Build helm packages"
echo "###################################################"

# make takes long time to complete
cd $HOME/oom/kubernetes

# nohup make all &&  make onap & # This takes more time for the LINT to complete
time make SKIP_LINT=TRUE all
time make SKIP_LINT=TRUE onap

echo "###################################################"
echo "Local helm packages"
echo "###################################################"
helm search -l


echo "###################################################"
echo "Kubernetes system PODs"
echo "###################################################"
kubectl get pods --all-namespaces


echo "###################################################"
echo "Completed setting up micork8s & ONAP helm charts"
echo "###################################################"

exit 0
