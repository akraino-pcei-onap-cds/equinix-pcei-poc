#!/bin/bash
#=======================================================================
# Copyright (c) 2017-2020 Aarna Networks, Inc.
# All rights reserved.
# ======================================================================
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#           http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ========================================================================

# You should have brought up micork8s on your host
# The below command will deploy ONAP frankfurt version of CDS

echo "###################################################"
echo "This script assumes that you have ONAP oom helm packages are built"
echo "are built and avaible. Also it assumes that you have"
echo " all the CLI tools kubectl and helm"
echo "###################################################"

echo "You can press Ctrl + c to stop this script"

sleep 20

echo "Going to deploy CDS"
helm upgrade --install dev local/onap --namespace onap \
-f /home/aarna/oom/kubernetes/onap/values.yaml \
--set cds.enabled=true \
--set mariadb-galera.enabled=true \
--set global.masterPassword=onap \
--set cds.cds-blueprints-processor.dmaapEnabled=false \
--set global.persistence.storageClass=microk8s-hostpath \
--set cds.cds-blueprints-processor.replicaCount=1 \
--set cds.cds-blueprints-processor.cluster.enabled=false

echo "###################################################"
echo "Printing all PODs under the namespace onap"
echo "###################################################"
kubectl get pods -n onap

exit 0
