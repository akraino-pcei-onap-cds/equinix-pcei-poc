# equinix-pcei-poc
This repository contains PCEI CDS blueprints and the utility scripts required for the Akrino demo.

# cds-blueprints folder contains CDS blueprints
  You can go to the individual CBA specific folder to find the step by step instructions.

# util-scripts folder contains utility scripts
  You can use the utility scripts to process and execute CBAs in this project

# You should deploy AMCOP2.0 to bring up EMCO and CDS components required for this demo
  You should follow AMCOP2.0 deployment guide to bring up all the required components in a single VM. You can request for a free AMCOP2.0 installation kit by filling up the online form https://docs.google.com/forms/d/e/1FAIpQLSc6PT4L5dRj9naytHTufEe1MW4DijoJAr8IFZep1bX9210RPw/viewform.

  You can also look at the AMCOP2.0 Quick Start Guide from the below link
  https://drive.google.com/file/d/1GK6nNLnfEjpW6vkYXx_GGnsbO39Y5rRW/view?usp=sharing

# Deploy AMCOP2.0 and clone this GIT repository under the $HOME folder within AMCOP VM

# You should follow the below steps to generate the gitlab.com access token required to execute CBAs
  1. Login to https://gitlab.com/
  2. Select the right corner **User settings** Select -> Preferences
  3. You can generate the access token by selecting the left hand side **Access tokens** menu as shown below
  
     ![](images/gitlab-access-token-screenshot.png)
  4. You should note down access token. 
  5. Please note you shoud give read only access to this token and select 1 day of expiry for this token
  
# You should update the CDS py-executor image required for this demo
  1. SSH into AMCOP2.0 VM
  2. Get the k8s deployments under ONAP name space

    $ kubectl get deployments -n onap
    NAME                           READY   UP-TO-DATE   AVAILABLE   AGE
    dev-cds-blueprints-processor   3/3     1            3           12m
    **dev-cds-py-executor**        1/1     1            1           12m
    dev-cds-sdc-listener           0/1     1            0           12m
    dev-cds-ui                     1/1     1            1           12m 

  3. Backup the CDS py-executor deployment

    $ kubectl get deployment -n onap dev-cds-py-executor -o yaml >~/cds-py-executor.yaml
  4. Apply the below patch to update the CDS py-executor image

    $ kubectl patch deployment -n onap dev-cds-py-executor -p '{"spec":{"template":{"spec":{"containers":[{"name":"cds-py-executor","image":"amcop/onap-f-ccsdk-py-executor-tf:0.7.6-SNAPSHOT-latest"}]}}}}'

  5. Backup the CDS blueprints-processor deployment

    $ kubectl get deployment -n onap dev-cds-blueprints-processor  -o yaml >~/cds-blueprints-processor.yaml

  6. Apply the below patch to update the CDS blueprint-processor image

    $ kubectl patch deployment -n onap dev-cds-blueprints-processor   -p '{"spec":{"template":{"spec":{"containers":[{"name":"cds-blueprints-processor","image":"amcop/onap-g-ccsdk-blueprintsprocessor:1.0.3-SNAPSHOT-latest"}]}}}}'

  7. Verify if we have CDS py-executor image updated

    $ kubectl describe pod -n onap dev-cds-py-executor | grep 'Image:'
    Image:         amcop/onap-f-ccsdk-py-executor-tf:0.7.6-SNAPSHOT-latest

  8. Verify if we have CDS py-executor image updated

    $ kubectl describe pod -n onap dev-cds-blueprints-processor | grep 'Image:'
    Image:          amcop/onap-g-ccsdk-blueprintsprocessor:1.0.3-SNAPSHOT-latest

  9. Wait for the CDS py-executor and blueprint-processor PODs transistion into running state.

    $ kubectl get pods -n onap | grep 'NAME\|py-executor\|blueprints-processor'
    NAME                                            READY   STATUS     RESTARTS   AGE
    dev-cds-blueprints-processor-64558d4f95-ff874   1/1     Running    0          46d
    dev-cds-blueprints-processor-64558d4f95-knqxk   1/1     Running    0          46d
    dev-cds-blueprints-processor-64558d4f95-zwbjr   1/1     Running    0          46d
    dev-cds-py-executor-77659bdc6-w4hnf             1/1     Running    0          45d

# You should now go to individual CDS cds-blueprints folder to enrich and load the CBAs into CDS runtime
  Please follow individual CBA specific steps in the README.md
