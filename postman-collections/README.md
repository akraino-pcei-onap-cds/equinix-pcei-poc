1. Login to postman.
  
    Create CDS variables in the postman Environment. 
    
    select  "No Environment" --> Environment quicklook icon --> ADD
      
    ![](images/postman-env.PNG)

2. Import json **cds-equinix.postman_collection.json**  
        
    ![](images/import-postman.PNG)

3. CDS execution steps
    1. Load boostrap to CDS 
    2. Load Data Dictionary into CDS
        Sample payload provider in postman collection Json.
        Multiple data dict must be loaded separately. 
    3. Enrich CBA 
            Create a zip and and load it through the postman.
            
        ![](images/enrich-postman.PNG)

            Save response to ZIP file

        ![](images/save-response.PNG)
            
    4. Save CBA 
            Use response zip file to load it through the postman.
            
        ![](images/save-cba.PNG)

    5. Get CBA list 
    6. Execute process.

        Update USER_INPUT_** attribute in the request payload before executing the process.
        
        Refer respective CBA project README.md for more details 
        

